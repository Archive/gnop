/* GnoP-Edit: A gui editor for the gnop files
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <glade/glade.h>
#include <gnop/gnop.h>
#include <gnop/gnopelement.h>
#include <gnop/gnopparser.h>
#include <gnop/gnop-type.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

static GnomeApp *app;
static GtkWidget *clist;
static GtkWidget *base_conf_path;

static GnopFile *gnop_file = NULL;
static GnopElement *selected_element = NULL;

void run_about_dialog(GtkWidget *w);
void file_open(GtkWidget *w);
void edit_add(GtkWidget *w);
void edit_properties(GtkWidget *w);

static struct {
	char *name;
	GnopType type;
} type_map[] = {
	{"Int", GNOP_TYPE_INT},
	{"Float", GNOP_TYPE_FLOAT},
	{"String", GNOP_TYPE_STRING},
	{"Bool", GNOP_TYPE_BOOL},
	{NULL, 0}
};
#define NUM_OF_TYPES 4

static char *
get_type_name(GConfValueType type)
{
	int i;
	for(i = 0; type_map[i].name != NULL; i++) {
		if(type_map[i].type == type)
			return type_map[i].name;
	}
	return NULL;
}

static int
get_type_index(GConfValueType type)
{
	int i;
	for(i = 0; type_map[i].name != NULL; i++) {
		if(type_map[i].type == type)
			return i;
	}
	return -1;
}

void
run_about_dialog(GtkWidget *w)
{
	GladeXML *xml;
	static GtkWidget *dialog = NULL;

	if(dialog) {
		gtk_widget_show_now(dialog);
		gdk_window_raise(dialog->window);
		return;
	}

	xml = glade_xml_new("gnop-edit.glade", "about");
	if(!xml) {
		g_warning("We could not load the interface!");
		return;
	}

	dialog = glade_xml_get_widget(xml, "about");
	gnome_dialog_set_parent(GNOME_DIALOG(dialog), GTK_WINDOW(app));

	gtk_signal_connect(GTK_OBJECT(dialog), "destroy",
			   GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			   &dialog);

	/* unref the xml file as it's not needed anymore */
	gtk_object_destroy(GTK_OBJECT(xml));
}

static void
add_element_to_clist(GnopElement *el, gpointer data)
{
	int row;
	char *text[3];

	text[0] = get_type_name(el->type);
	text[1] = el->widget;
	text[2] = el->conf_path;

	row = gtk_clist_append(GTK_CLIST(clist), text);
	gtk_clist_set_row_data(GTK_CLIST(clist), row, el);
}

static void
refresh_window(void)
{
	gtk_clist_freeze(GTK_CLIST(clist));
	gtk_clist_clear(GTK_CLIST(clist));

	gnop_file_foreach_element(gnop_file, add_element_to_clist, NULL);

	gtk_clist_thaw(GTK_CLIST(clist));

	gtk_entry_set_text(GTK_ENTRY(base_conf_path),
			   gnop_file->base_conf_path);
}

static void
file_open_really(GtkWidget *w, gpointer data)
{
	GtkFileSelection *fsel = data;
	GnopFile *file;
	char *filename;

	filename = gtk_file_selection_get_filename(fsel);

	if( ! g_file_exists(filename)) {
		GtkWidget *w = gnome_error_dialog("File does not exist");
		gnome_dialog_set_parent(GNOME_DIALOG(w), GTK_WINDOW(fsel));
		return;
	}

	file = gnop_file_load_xml(filename);

	if( ! file) {
		GtkWidget *w = gnome_error_dialog("File cannot be loaded");
		gnome_dialog_set_parent(GNOME_DIALOG(w), GTK_WINDOW(fsel));
		return;
	}

	if(gnop_file)
		gnop_file_unref(gnop_file);
	gnop_file = file;
	selected_element = NULL;

	refresh_window();

	gtk_widget_destroy(GTK_WIDGET(fsel));
}

void
file_open(GtkWidget *w)
{
	static GtkWidget *dialog = NULL;
	GtkFileSelection *fsel;

	if(dialog) {
		gtk_widget_show_now(dialog);
		gdk_window_raise(dialog->window);
		return;
	}

	dialog = gtk_file_selection_new("Open");
	fsel = GTK_FILE_SELECTION(dialog);

	gtk_signal_connect(GTK_OBJECT(dialog), "destroy",
			   GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			   &dialog);

	gtk_signal_connect(GTK_OBJECT(fsel->ok_button), "clicked",
			   GTK_SIGNAL_FUNC(file_open_really),
			   fsel);

	gtk_signal_connect_object(GTK_OBJECT(fsel->cancel_button), "clicked",
				  GTK_SIGNAL_FUNC(gtk_widget_destroy),
				  GTK_OBJECT(fsel));

	gtk_widget_show(dialog);
}

static GtkWidget *
gnop_gtk_option_menu_get_item (GtkOptionMenu *option_menu, int index)
{
  g_return_val_if_fail (option_menu != NULL, NULL);
  g_return_val_if_fail (GTK_IS_OPTION_MENU (option_menu), NULL);

  if (option_menu->menu)
    {
      return g_list_nth_data(GTK_MENU_SHELL(option_menu->menu)->children,
			     index);
    }
  return NULL;
}

static int
gnop_gtk_option_menu_get_history (GtkOptionMenu *option_menu)
{
  GtkWidget *menu_item;

  g_return_val_if_fail (option_menu != NULL, -1);
  g_return_val_if_fail (GTK_IS_OPTION_MENU (option_menu), -1);

  if (option_menu->menu)
    {
      menu_item = gtk_menu_get_active (GTK_MENU (option_menu->menu));
      return g_list_index(GTK_MENU_SHELL(option_menu->menu)->children,
			  menu_item);
    }
  return -1;
}

static void
change_type(GtkWidget *item, gpointer data)
{
	GnopElement *el = data;
	GtkWidget *option_menu;
	int i;

	option_menu = gtk_object_get_user_data(GTK_OBJECT(item));
	i = gnop_gtk_option_menu_get_history(GTK_OPTION_MENU(option_menu));

	gnop_element_set_type(el, type_map[i].type);

	refresh_window();
}

static void
change_widget(GtkWidget *entry, gpointer data)
{
	GnopElement *el = data;

	gnop_element_set_widget(el, gtk_entry_get_text(GTK_ENTRY(entry)));

	refresh_window();
}

static void
change_conf_path(GtkWidget *entry, gpointer data)
{
	GnopElement *el = data;

	gnop_element_set_conf_path(el, gtk_entry_get_text(GTK_ENTRY(entry)));

	refresh_window();
}

static void
setup_property_dialog(GladeXML *xml, GnopElement *el)
{
	GtkWidget *w, *item;
	int i;

	w = glade_xml_get_widget(xml, "basic_type");
	i = get_type_index(el->type);
	gtk_option_menu_set_history(GTK_OPTION_MENU(w), i);
	item = gnop_gtk_option_menu_get_item(GTK_OPTION_MENU(w), i);
	gtk_menu_item_activate(GTK_MENU_ITEM(item));

	/* setup the type changing to work */
	for(i=0; i<NUM_OF_TYPES; i++) {
		item = gnop_gtk_option_menu_get_item(GTK_OPTION_MENU(w), i);
		gtk_object_set_user_data(GTK_OBJECT(item), w);
		gtk_signal_connect_after(GTK_OBJECT(item), "activate",
					 GTK_SIGNAL_FUNC(change_type),
					 el);
	}

	w = glade_xml_get_widget(xml, "basic_widget");
	gtk_entry_set_text(GTK_ENTRY(w), el->widget?el->widget:"");
	gtk_signal_connect_after(GTK_OBJECT(w), "changed",
				 GTK_SIGNAL_FUNC(change_widget),
				 el);

	w = glade_xml_get_widget(xml, "basic_conf_path");
	gtk_entry_set_text(GTK_ENTRY(w), el->conf_path);
	gtk_signal_connect_after(GTK_OBJECT(w), "changed",
				 GTK_SIGNAL_FUNC(change_conf_path),
				 el);
}

void
edit_add(GtkWidget *w)
{
	/*FIXME: */
}

static void
property_was_destroyed(GtkWidget *w, gpointer data)
{
	GnopElement *el = data;

	gnop_element_set_data(el, NULL, NULL);
}

void
edit_properties(GtkWidget *w)
{
	GladeXML *xml;
	GtkWidget *dialog;

	if(!selected_element) {
		gnome_app_error(app, "No element selected");
		return;
	}

	/* this element has a data associated */
	if(selected_element->data) {
		dialog = selected_element->data;
		gtk_widget_show_now(dialog);
		gdk_window_raise(dialog->window);
		return;
	}

	xml = glade_xml_new("gnop-edit.glade", "key_property");
	if(!xml) {
		g_warning("We could not load the interface!");
		return;
	}

	dialog = glade_xml_get_widget(xml, "property");
	gnome_dialog_set_parent(GNOME_DIALOG(dialog), GTK_WINDOW(app));

	gtk_signal_connect(GTK_OBJECT(dialog), "destroy",
			   GTK_SIGNAL_FUNC(property_was_destroyed),
			   selected_element);
	gtk_signal_connect_object(GTK_OBJECT(dialog), "destroy",
				  GTK_SIGNAL_FUNC(gtk_object_destroy),
				  GTK_OBJECT(xml));

	gnop_element_set_data(selected_element, dialog, (GDestroyNotify)gtk_widget_destroy);

	setup_property_dialog(xml, selected_element);
}

static void
clist_select_row(GtkCList *clist, int row, int column, GdkEvent *event,
		 gpointer data)
{
	selected_element = gtk_clist_get_row_data(clist, row);
}

static void
clist_unselect_row(GtkCList *clist, int row, int column, GdkEvent *event,
		   gpointer data)
{
	selected_element = NULL;
}


int
main(int argc, char *argv[])
{
	GladeXML *xml;
	GtkWidget *w;

	gnome_init("gnop-edit", VERSION, argc, argv);
	glade_gnome_init();
	gnop_init(argc, argv);

	w = gnome_ok_dialog("This editor is not finished and cannot edit GnoP files!\n"
			    "Be careful with it and for now edit gnop files with a text editor.");
	gnome_dialog_run(GNOME_DIALOG(w));
	
	xml = glade_xml_new("gnop-edit.glade", "app");
	if( ! xml) {
		g_warning("We could not load the interface!");
		return 1;
	}

	app = GNOME_APP(glade_xml_get_widget(xml, "app"));
	clist = glade_xml_get_widget(xml, "keys_clist");
	base_conf_path = glade_xml_get_widget(xml, "base_conf_path");

	gtk_signal_connect(GTK_OBJECT(clist), "select_row",
			   GTK_SIGNAL_FUNC(clist_select_row),
			   NULL);
	gtk_signal_connect(GTK_OBJECT(clist), "unselect_row",
			   GTK_SIGNAL_FUNC(clist_unselect_row),
			   NULL);

	glade_xml_signal_autoconnect(xml);

	gtk_object_destroy(GTK_OBJECT(xml));

	gtk_main();

	return 0;
}
