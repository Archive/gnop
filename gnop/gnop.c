/* GnoP: the library interface
 * Author: George Lebl
 * (c) 1999 Free Software Foundation
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>

#include "gnopelement.h"
#include "gnopparser.h"
#include "gnoppane.h"

#include "gnop-native-widgets.h"

#include "gnop.h"

GList *gnop_directories = NULL;

static gboolean gnop_initialized = FALSE;

gboolean
gnop_is_initialized(void)
{
	return gnop_initialized;
}

gboolean
gnop_init(gint argc, gchar **argv)
{
	GConfError *error = NULL;

	/* Allow multiple calls to gnop_init */
	if(gnop_initialized)
		return TRUE;

	if ( ! gconf_is_initialized() &&
	     ! gconf_init(argc, argv, &error)) {
		g_assert(error != NULL);
		g_warning(_("GConf init failed:\n  %s"), error->str);
		gconf_error_destroy(error);
		return FALSE;
	}

	gnop_initialized = TRUE;

	return TRUE;
}

/* An init that can be run at any time */
void
gnop_lazy_init(void)
{
	gboolean lazy_init_run = FALSE;

	if(lazy_init_run)
		return;

	/* Make sure these native widget classes are created just for the sake
	 * of non-gmodule supporting systems */
	gnop_radio_group_get_type();
	gnop_check_group_get_type();
	gnop_spin_button_get_type();
}


void
gnop_free_unused_data(void)
{
	gnop_element_free_unused_data();
	gnop_parser_free_unused_data();
	gnop_pane_free_unused_data();
}

void
gnop_add_directory(const char *directory)
{
	gnop_directories = g_list_append(gnop_directories, g_strdup(directory));
}

/* Pure sugar functions, this way adding a config dialog is a 1-liner */
GtkWidget *
gnop_run_simple_dialog(const char *file)
{
	GtkWidget *dlg;
	GnopXML *xml = gnop_xml_new(file);
	if( ! xml) {
		g_warning(_("Cannot load config dialog"));
		return NULL;
	}
	gnop_xml_show_dialog(xml);
	dlg = gnop_xml_get_dialog_widget(xml);

	gtk_object_unref(GTK_OBJECT(xml));

	if( ! dlg) {
		g_warning(_("Cannot run config dialog"));
		return NULL;
	}
	return dlg;
}

GtkWidget *
gnop_run_simple_dialog_from_memory(const char *buffer, int len)
{
	GtkWidget *dlg;
	GnopXML *xml = gnop_xml_new_from_memory(buffer, len);
	if( ! xml) {
		g_warning(_("Cannot load config dialog"));
		return NULL;
	}
	gnop_xml_show_dialog(xml);
	dlg = gnop_xml_get_dialog_widget(xml);

	gtk_object_unref(GTK_OBJECT(xml));

	if( ! dlg) {
		g_warning(_("Cannot run config dialog"));
		return NULL;
	}
	return dlg;
}
