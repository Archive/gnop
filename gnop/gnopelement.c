/* GnoP: the elements interface
 * Author: George Lebl
 * (c) 1999 Free Software Foundation
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "config.h"
#include <gnome.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "gnopwidgetutil.h"
#include "gnop-widget-interface.h"

#include "gnoputil.h"
#include "gnopelement.h"

static void gnop_element_destroy(GnopElement *ge);

static GHashTable *type_hash = NULL;

static void
init_type_hash(void)
{
	if(type_hash) return;

	type_hash = g_hash_table_new(g_str_hash, g_str_equal);

	/* Basic types */
	g_hash_table_insert(type_hash, "String",
			    GINT_TO_POINTER(GNOP_TYPE_STRING));
	g_hash_table_insert(type_hash, "Int",
			    GINT_TO_POINTER(GNOP_TYPE_INT));
	g_hash_table_insert(type_hash, "Float",
			    GINT_TO_POINTER(GNOP_TYPE_FLOAT));
	g_hash_table_insert(type_hash, "Bool",
			    GINT_TO_POINTER(GNOP_TYPE_BOOL));

	/* List types */
	g_hash_table_insert(type_hash, "ListOfStrings",
			    GINT_TO_POINTER(GNOP_TYPE_LIST_OF_STRINGS));
	g_hash_table_insert(type_hash, "ListOfInts",
			    GINT_TO_POINTER(GNOP_TYPE_LIST_OF_INTS));
	g_hash_table_insert(type_hash, "ListOfFloats",
			    GINT_TO_POINTER(GNOP_TYPE_LIST_OF_FLOATS));
	g_hash_table_insert(type_hash, "ListOfBools",
			    GINT_TO_POINTER(GNOP_TYPE_LIST_OF_BOOLS));

	/* Pair types */
	g_hash_table_insert(type_hash, "PairStringString",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_STRING_STRING));
	g_hash_table_insert(type_hash, "PairStringInt",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_STRING_INT));
	g_hash_table_insert(type_hash, "PairStringFloat",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_STRING_FLOAT));
	g_hash_table_insert(type_hash, "PairStringBool",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_STRING_BOOL));

	g_hash_table_insert(type_hash, "PairIntString",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_INT_STRING));
	g_hash_table_insert(type_hash, "PairIntInt",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_INT_INT));
	g_hash_table_insert(type_hash, "PairIntFloat",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_INT_FLOAT));
	g_hash_table_insert(type_hash, "PairIntBool",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_INT_BOOL));

	g_hash_table_insert(type_hash, "PairFloatString",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_FLOAT_STRING));
	g_hash_table_insert(type_hash, "PairFloatInt",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_FLOAT_INT));
	g_hash_table_insert(type_hash, "PairFloatFloat",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_FLOAT_FLOAT));
	g_hash_table_insert(type_hash, "PairFloatBool",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_FLOAT_BOOL));

	g_hash_table_insert(type_hash, "PairBoolString",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_BOOL_STRING));
	g_hash_table_insert(type_hash, "PairBoolInt",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_BOOL_INT));
	g_hash_table_insert(type_hash, "PairBoolFloat",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_BOOL_FLOAT));
	g_hash_table_insert(type_hash, "PairBoolBool",
			    GINT_TO_POINTER(GNOP_TYPE_PAIR_BOOL_BOOL));
}

static GnopType
get_type(const char *type_string)
{
	g_return_val_if_fail(type_string != NULL, 0);

	init_type_hash();

	return GPOINTER_TO_INT(g_hash_table_lookup(type_hash, type_string));
}

GnopElement *
gnop_element_new(void)
{
	GnopElement *ge;
       
	ge = g_new0(GnopElement, 1);
	ge->refcount = 1;

	return ge;
}

void
gnop_element_ref(GnopElement *ge)
{
	g_return_if_fail(ge != NULL);

	ge->refcount++;
}

void
gnop_element_unref(GnopElement *ge)
{
	g_return_if_fail(ge != NULL);

	ge->refcount--;
	if(ge->refcount<=0)
		gnop_element_destroy(ge);
}

static void
gnop_element_destroy(GnopElement *ge)
{
	g_return_if_fail(ge != NULL);

	/* called before ANY destruction on the object is done so that
	 * the object is still completely intact */
	if(ge->destroy_notify)
		ge->destroy_notify(ge->data);
	ge->destroy_notify = NULL;
	ge->data = NULL;

	g_free(ge->conf_path);
	ge->conf_path = NULL;

	g_slist_foreach(ge->sensitivity, (GFunc)gnop_sensitivity_destroy, NULL);
	g_slist_free(ge->sensitivity);
	ge->sensitivity = NULL;

	g_free(ge);
}

void
gnop_element_set_type(GnopElement *el, GnopType type)
{
	g_return_if_fail(el != NULL);
	g_return_if_fail(type >= 0);

	el->type = type;
}

void
gnop_element_set_type_string(GnopElement *el, const char *type)
{
	g_return_if_fail(el != NULL);
	g_return_if_fail(type != NULL);

	el->type = get_type(type);
}

void
gnop_element_set_data(GnopElement *el,
		      gpointer data,
		      GDestroyNotify destroy_notify)
{
	g_return_if_fail(el != NULL);

	if(el->destroy_notify) {
		el->destroy_notify(el->data);
	}

	el->data = data;
	el->destroy_notify = destroy_notify;
}

void
gnop_element_set_widget(GnopElement *el, const char *widget)
{
	g_return_if_fail(el != NULL);
	g_return_if_fail(widget != NULL);

	g_free(el->widget);
	el->widget = g_strdup(widget);
}

void
gnop_element_set_conf_path(GnopElement *el, const char *conf_path)
{
	g_return_if_fail(el != NULL);
	g_return_if_fail(conf_path != NULL);

	g_free(el->conf_path);
	el->conf_path = g_strdup(conf_path);
}

/* Note: takes ownership of 'sens' */
void
gnop_element_add_sensitivity(GnopElement *ge, GnopSensitivity *sens)
{
	g_return_if_fail(ge != NULL);
	g_return_if_fail(sens != NULL);

	ge->sensitivity = g_slist_append(ge->sensitivity, sens);
}


void
gnop_element_free_unused_data(void)
{
	if(type_hash) {
		g_hash_table_destroy(type_hash);
		type_hash = NULL;
	}
}

GnopRuntimeElement *
gnop_runtime_element_new(GnopElement *el, GnopRuntimeUpdateFunc func,
			 gpointer data)
{
	GnopRuntimeElement *gre = g_new0(GnopRuntimeElement, 1);
	gre->el = el;
	gre->gconf_notify = 0;
	gre->suspend_gconf_notify = FALSE;
	gre->updated = FALSE;
	gre->suspend_widget_update = FALSE;
	gre->updated_idle = 0;
	gre->update_func = func;
	gre->update_data = data;

	/* we let GnopXML set up the pointers for us */
	return gre;
}

void
gnop_runtime_element_destroy(GnopRuntimeElement *gre, GConfClient *client)
{
	g_return_if_fail(gre != NULL);
	g_return_if_fail(client != NULL);
	g_return_if_fail(GCONF_IS_CLIENT(client));

	gnop_runtime_element_remove_notify(gre, client);

	if(gre->updated_idle > 0)
		gtk_idle_remove(gre->updated_idle);
	gre->updated_idle = 0;

	g_free(gre);
}

void
gnop_runtime_element_add_notify(GnopRuntimeElement *gre,
				GConfClient *client,
				const char *prefix,
				GConfClientNotifyFunc func,
				gpointer user_data,
				GFreeFunc destroy_notify,
				GConfError **err)
{
	char *fullkey;

	g_return_if_fail(gre != NULL);
	g_return_if_fail(client != NULL);
	g_return_if_fail(GCONF_IS_CLIENT(client));

	if(prefix && gre->el->conf_path[0] != '/')
		fullkey = g_concat_dir_and_file(prefix, gre->el->conf_path);
	else
		fullkey = gre->el->conf_path;

	gnop_runtime_element_remove_notify(gre, client);

	gre->gconf_notify = gconf_client_notify_add(client, fullkey,
						    func, user_data,
						    destroy_notify, err);
	if(gre->gconf_notify != 0 &&
	   gre->el->conf_path[0] == '/') {
		char *dir = g_dirname(gre->el->conf_path);
		if( ! *dir || ! dir)
			gconf_client_add_dir(client, "/",
					     GCONF_CLIENT_PRELOAD_NONE, NULL);
		else
			gconf_client_add_dir(client, dir,
					     GCONF_CLIENT_PRELOAD_NONE, NULL);
		g_free(dir);
	}

	if(prefix && gre->el->conf_path[0] != '/')
		g_free(fullkey);
}

void
gnop_runtime_element_remove_notify(GnopRuntimeElement *gre,
				   GConfClient *client)
{
	g_return_if_fail(gre != NULL);
	g_return_if_fail(client != NULL);
	g_return_if_fail(GCONF_IS_CLIENT(client));

	if(gre->gconf_notify > 0) {
		gconf_client_notify_remove(client, gre->gconf_notify);
		if(gre->el->conf_path[0] == '/') {
			char *dir = g_dirname(gre->el->conf_path);
			if( ! *dir || ! dir)
				gconf_client_remove_dir(client, "/", NULL);
			else
				gconf_client_remove_dir(client, dir, NULL);
			g_free(dir);
		}
	}
	gre->gconf_notify = 0;
}

static gboolean
widget_change_idle(gpointer data)
{
	GnopRuntimeElement *gre = data;

	gre->updated_idle = 0;

	if(gre->suspend_widget_update)
		gre->suspend_widget_update = FALSE;
	else
		gre->updated = TRUE;

	if(gre->update_func)
		gre->update_func(gre, gre->update_data);

	return FALSE;
}


static void
widget_change_signal(GtkWidget *widget, gpointer data)
{
	GnopRuntimeElement *gre = data;

	if(gre->updated_idle == 0)
		gre->updated_idle = gtk_idle_add(widget_change_idle, gre);
}

static void
calculator_change_signal(GtkWidget *widget, double result, gpointer data)
{
	GnopRuntimeElement *gre = data;

	if(gre->updated_idle == 0)
		gre->updated_idle = gtk_idle_add(widget_change_idle, gre);
}

void
gnop_runtime_element_setup_handlers(GnopRuntimeElement *gre)
{
	GtkWidget *w;

	g_return_if_fail(gre != NULL);
	g_return_if_fail(gre->widget != NULL);
	g_return_if_fail(GTK_IS_WIDGET(gre->widget));
	g_return_if_fail(gre->widget_type != GNOP_INVALIDWIDGET);

	/* we are resetting the windget updaters here */
	gre->suspend_widget_update = FALSE;

	switch(gre->widget_type) {
	case GNOP_NATIVEWIDGET:
		g_assert(gnop_widget_interface_exists(gre->widget));
		gtk_signal_connect_after
			(GTK_OBJECT(gre->widget),
			 gnop_widget_interface_changed_signal(gre->widget),
			 GTK_SIGNAL_FUNC(widget_change_signal),
			 gre);
		break;
	case GNOP_GTKSPINBUTTON:
		gtk_signal_connect_after(GTK_OBJECT(gre->widget),
					 "changed",
					 GTK_SIGNAL_FUNC(widget_change_signal),
					 gre);
		break;
	case GNOP_GTKENTRY:
		gtk_signal_connect_after(GTK_OBJECT(gre->widget),
					 "changed",
					 GTK_SIGNAL_FUNC(widget_change_signal),
					 gre);
		break;
	case GNOP_GTKCOMBO:
		gtk_signal_connect_after(GTK_OBJECT(GTK_COMBO(gre->widget)->entry),
					 "changed",
					 GTK_SIGNAL_FUNC(widget_change_signal),
					 gre);
		break;
	case GNOP_GNOMEENTRY:
		w = gnome_entry_gtk_entry(GNOME_ENTRY(gre->widget));
		gtk_signal_connect_after(GTK_OBJECT(w),
					 "changed",
					 GTK_SIGNAL_FUNC(widget_change_signal),
					 gre);
		break;
	case GNOP_GNOMEFILEENTRY:
		w = gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(gre->widget));
		gtk_signal_connect_after(GTK_OBJECT(w),
					 "changed",
					 GTK_SIGNAL_FUNC(widget_change_signal),
					 gre);
		break;
	case GNOP_GNOMEPIXMAPENTRY:
		w = gnome_pixmap_entry_gtk_entry(GNOME_PIXMAP_ENTRY(gre->widget));
		gtk_signal_connect_after(GTK_OBJECT(w),
					 "changed",
					 GTK_SIGNAL_FUNC(widget_change_signal),
					 gre);
		break;
	case GNOP_GNOMEICONENTRY:
		w = gnome_icon_entry_gtk_entry(GNOME_ICON_ENTRY(gre->widget));
		gtk_signal_connect_after(GTK_OBJECT(w),
					 "changed",
					 GTK_SIGNAL_FUNC(widget_change_signal),
					 gre);
		break;
	case GNOP_GNOMENUMBERENTRY:
		w = gnome_number_entry_gtk_entry(GNOME_NUMBER_ENTRY(gre->widget));
		gtk_signal_connect_after(GTK_OBJECT(w),
					 "changed",
					 GTK_SIGNAL_FUNC(widget_change_signal),
					 gre);
		break;
	case GNOP_GTKTOGGLEBUTTON:
		gtk_signal_connect_after(GTK_OBJECT(gre->widget),
					 "toggled",
					 GTK_SIGNAL_FUNC(widget_change_signal),
					 gre);
		break;
	case GNOP_GNOMECALCULATOR:
		gtk_signal_connect_after(GTK_OBJECT(gre->widget),
					 "result_changed",
					 GTK_SIGNAL_FUNC(calculator_change_signal),
					 gre);
		break;
	default:
		g_warning(_("Invalid string widget type"));
		break;
	}
}

gboolean
gnop_runtime_element_set_widget(GnopRuntimeElement *gre, GtkWidget *widget)
{
	g_return_val_if_fail(gre != NULL, FALSE);
	g_return_val_if_fail(widget != NULL, FALSE);
	g_return_val_if_fail(GTK_IS_WIDGET(widget), FALSE);

	gre->widget = widget;
	gre->widget_type = GNOP_INVALIDWIDGET;
	gtk_signal_connect(GTK_OBJECT(gre->widget), "destroy",
			   GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			   &gre->widget);

	/* figure out the type of the widget */
	if(gnop_widget_interface_exists(widget))
		gre->widget_type = GNOP_NATIVEWIDGET;
	else if(GTK_IS_SPIN_BUTTON(widget))
		gre->widget_type = GNOP_GTKSPINBUTTON;
	else if(GTK_IS_ENTRY(widget))
		gre->widget_type = GNOP_GTKENTRY;
	else if(GTK_IS_TOGGLE_BUTTON(widget))
		gre->widget_type = GNOP_GTKTOGGLEBUTTON;
	else if(GNOME_IS_NUMBER_ENTRY(widget))
		gre->widget_type = GNOP_GNOMENUMBERENTRY;
	else if(GNOME_IS_FILE_ENTRY(widget))
		gre->widget_type = GNOP_GNOMEFILEENTRY;
	else if(GNOME_IS_PIXMAP_ENTRY(widget))
		gre->widget_type = GNOP_GNOMEPIXMAPENTRY;
	else if(GNOME_IS_ICON_ENTRY(widget))
		gre->widget_type = GNOP_GNOMEICONENTRY;
	else if(GNOME_IS_ENTRY(widget))
		gre->widget_type = GNOP_GNOMEENTRY;
	else if(GNOME_IS_CALCULATOR(widget))
		gre->widget_type = GNOP_GNOMECALCULATOR;
	else if(GTK_IS_COMBO(widget)) /* must be after gnome_entry! */
		gre->widget_type = GNOP_GTKCOMBO;

	if(gre->widget_type == GNOP_INVALIDWIDGET)
		return FALSE;

	return TRUE;
}

void
gnop_runtime_element_key_to_widget(GnopRuntimeElement *gre,
				   GConfClient *client,
				   const char *prefix)
{
	char *fullkey;
	GConfValue *value;

	g_return_if_fail(gre != NULL);

	gre->suspend_widget_update = TRUE;

	g_return_if_fail(gre != NULL);
	g_return_if_fail(client != NULL);
	g_return_if_fail(GCONF_IS_CLIENT(client));

	if(prefix && gre->el->conf_path[0] != '/')
		fullkey = g_concat_dir_and_file(prefix, gre->el->conf_path);
	else
		fullkey = gre->el->conf_path;

	value = gconf_client_get(client, fullkey, NULL);
	if(value) {
		/* This will not set a value if this value is already in the
		 * widget, thus preventing infinite loops */
		gnop_runtime_element_set_value(gre, value);
		gconf_value_destroy(value);
	}

	if(prefix && gre->el->conf_path[0] != '/')
		g_free(fullkey);
}

void
gnop_runtime_element_widget_to_key(GnopRuntimeElement *gre,
				   GConfClient *client,
				   const char *prefix)
{
	char *fullkey;
	GConfValue *value;

	g_return_if_fail(gre != NULL);

	gre->suspend_gconf_notify = TRUE;


	g_return_if_fail(gre != NULL);
	g_return_if_fail(client != NULL);
	g_return_if_fail(GCONF_IS_CLIENT(client));

	value = gnop_runtime_element_get_value(gre);
	if( ! value) {
		g_warning(_("Error while getting value from widget"));
		return;
	}

	if(prefix && gre->el->conf_path[0] != '/')
		fullkey = g_concat_dir_and_file(prefix, gre->el->conf_path);
	else
		fullkey = gre->el->conf_path;

	gconf_client_set(client, fullkey, value, NULL);
	gre->updated = FALSE;

	g_free(value);

	if(prefix && gre->el->conf_path[0] != '/')
		g_free(fullkey);
}

static GConfValue *
gnop_runtime_element_get_int(GnopRuntimeElement *gre)
{
	GConfValue *value = NULL;

	g_return_val_if_fail(gre != NULL, NULL);

	switch(gre->widget_type) {
	case GNOP_GTKSPINBUTTON:
		value = gconf_value_new(GCONF_VALUE_INT);
		gconf_value_set_int(value,
				    gtk_spin_button_get_value_as_int
				    (GTK_SPIN_BUTTON(gre->widget)));
		return value;
	case GNOP_GNOMENUMBERENTRY:
		value = gconf_value_new(GCONF_VALUE_INT);
		gconf_value_set_int(value,
				    gnome_number_entry_get_number
				    (GNOME_NUMBER_ENTRY(gre->widget)));
		return value;
	case GNOP_GNOMECALCULATOR:
		value = gconf_value_new(GCONF_VALUE_INT);
		gconf_value_set_int(value,
				    gnome_calculator_get_result
				    (GNOME_CALCULATOR(gre->widget)));
		return value;
	case GNOP_GTKENTRY:
		value = gconf_value_new(GCONF_VALUE_INT);
		gconf_value_set_int(value,
				    gnop_gtk_entry_get_long(GTK_ENTRY(gre->widget)));
		return value;
	case GNOP_GTKCOMBO:
		value = gconf_value_new(GCONF_VALUE_INT);
		gconf_value_set_int(value,
				    gnop_gtk_entry_get_long
				    (GTK_ENTRY(GTK_COMBO(gre->widget)->entry)));
		return value;
	case GNOP_GNOMEENTRY:
		value = gconf_value_new(GCONF_VALUE_INT);
		gconf_value_set_int(value,
				    gnop_gnome_entry_get_long
				    (GNOME_ENTRY(gre->widget)));
		return value;
	default:
		g_warning(_("Invalid integer widget type"));
		return NULL;
	}
}

static GConfValue *
gnop_runtime_element_get_float(GnopRuntimeElement *gre)
{
	GConfValue *value = NULL;

	g_return_val_if_fail(gre != NULL, NULL);

	switch(gre->widget_type) {
	case GNOP_GTKSPINBUTTON:
		value = gconf_value_new(GCONF_VALUE_FLOAT);
		gconf_value_set_float(value,
				      gtk_spin_button_get_value_as_float
				      (GTK_SPIN_BUTTON(gre->widget)));
		return value;
	case GNOP_GNOMENUMBERENTRY:
		value = gconf_value_new(GCONF_VALUE_FLOAT);
		gconf_value_set_float(value,
				      gnome_number_entry_get_number
				      (GNOME_NUMBER_ENTRY(gre->widget)));
		return value;
	case GNOP_GNOMECALCULATOR:
		value = gconf_value_new(GCONF_VALUE_FLOAT);
		gconf_value_set_float(value,
				      gnome_calculator_get_result
				      (GNOME_CALCULATOR(gre->widget)));
		return value;
	case GNOP_GTKENTRY:
		value = gconf_value_new(GCONF_VALUE_FLOAT);
		gconf_value_set_float(value,
				      gnop_gtk_entry_get_double
				      (GTK_ENTRY(gre->widget)));
		return value;
	case GNOP_GTKCOMBO:
		value = gconf_value_new(GCONF_VALUE_FLOAT);
		gconf_value_set_float(value,
				      gnop_gtk_entry_get_double
				      (GTK_ENTRY(GTK_COMBO(gre->widget)->entry)));
		return value;
	case GNOP_GNOMEENTRY:
		value = gconf_value_new(GCONF_VALUE_FLOAT);
		gconf_value_set_float(value,
				      gnop_gnome_entry_get_double
				      (GNOME_ENTRY(gre->widget)));
		return value;
	default:
		g_warning(_("Invalid double widget type"));
		return NULL;
	}
}

static GConfValue *
gnop_runtime_element_get_string(GnopRuntimeElement *gre)
{
	GConfValue *value = NULL;

	g_return_val_if_fail(gre != NULL, NULL);

	switch(gre->widget_type) {
	case GNOP_GTKENTRY:
		value = gconf_value_new(GCONF_VALUE_STRING);
		gconf_value_set_string(value,
				       gtk_entry_get_text(GTK_ENTRY(gre->widget)));
		return value;
	case GNOP_GTKCOMBO:
		value = gconf_value_new(GCONF_VALUE_STRING);
		gconf_value_set_string(value,
				       gtk_entry_get_text
				       (GTK_ENTRY(GTK_COMBO(gre->widget)->entry)));
		return value;
	case GNOP_GNOMEENTRY:
		value = gconf_value_new(GCONF_VALUE_STRING);
		gconf_value_set_string(value,
				       gnop_gnome_entry_get_text(GNOME_ENTRY(gre->widget)));
		return value;
	case GNOP_GNOMEFILEENTRY:
		value = gconf_value_new(GCONF_VALUE_STRING);
		gconf_value_set_string(value,
				       gnop_gnome_file_entry_get_text
				       (GNOME_FILE_ENTRY(gre->widget)));
		return value;
	case GNOP_GNOMEPIXMAPENTRY:
		value = gconf_value_new(GCONF_VALUE_STRING);
		gconf_value_set_string(value,
				       gnop_gnome_pixmap_entry_get_text
				       (GNOME_PIXMAP_ENTRY(gre->widget)));
		return value;
	case GNOP_GNOMEICONENTRY:
		value = gconf_value_new(GCONF_VALUE_STRING);
		gconf_value_set_string(value,
				       gnop_gnome_icon_entry_get_text
				       (GNOME_ICON_ENTRY(gre->widget)));
		return value;
	default:
		g_warning(_("Invalid string widget type"));
		return NULL;
	}
}

static GConfValue *
gnop_runtime_element_get_bool(GnopRuntimeElement *gre)
{
	GConfValue *value = NULL;

	g_return_val_if_fail(gre != NULL, NULL);

	switch(gre->widget_type) {
	case GNOP_GTKTOGGLEBUTTON:
		value = gconf_value_new(GCONF_VALUE_BOOL);
		gconf_value_set_bool(value,
				     GTK_TOGGLE_BUTTON(gre->widget)->active != FALSE);
		return value;
	default:
		g_warning(_("Invalid boolean widget type"));
		return NULL;
	}
}

static GConfValue *
gnop_runtime_element_get_weird(GnopRuntimeElement *gre)
{
	g_return_val_if_fail(gre != NULL, NULL);

	switch(gre->widget_type) {
	case GNOP_GTKENTRY:
		return gnop_gconf_value_from_string
			(gtk_entry_get_text(GTK_ENTRY(gre->widget)),
			 gre->el->type);
	case GNOP_GTKCOMBO:
		return gnop_gconf_value_from_string
			(gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(gre->widget)->entry)),
			 gre->el->type);
	case GNOP_GNOMEENTRY:
		return gnop_gconf_value_from_string
			(gnop_gnome_entry_get_text(GNOME_ENTRY(gre->widget)),
			 gre->el->type);
	default:
		g_warning(_("Invalid list/pair widget type"));
		return NULL;
	}
}

GConfValue *
gnop_runtime_element_get_value(GnopRuntimeElement *gre)
{
	GConfValue *value;

	g_return_val_if_fail(gre != NULL, NULL);

	if(gre->widget_type == GNOP_NATIVEWIDGET) {
		value = NULL;
		if( ! gnop_widget_interface_get_value(gre->widget, gre->el->type, &value))
			g_warning(_("Error getting string from widget"));
		return value;
	}

	/* Non native widgets, only supported types need to be here */
	switch(gre->el->type) {
	case GNOP_TYPE_STRING:
		value = gnop_runtime_element_get_string(gre);
		break;
	case GNOP_TYPE_INT:
		value = gnop_runtime_element_get_int(gre);
		break;
	case GNOP_TYPE_FLOAT:
		value = gnop_runtime_element_get_float(gre);
		break;
	case GNOP_TYPE_BOOL:
		value = gnop_runtime_element_get_bool(gre);
		break;
	default:
		value = gnop_runtime_element_get_weird(gre);
		break;
	}

	return value;
}

static void
gnop_runtime_element_set_int(GnopRuntimeElement *gre, GConfValue *value)
{
	g_return_if_fail(gre != NULL);
	g_return_if_fail(value != NULL);
	g_return_if_fail(value->type == GNOP_TYPE_INT);

	switch(gre->widget_type) {
	case GNOP_GTKSPINBUTTON:
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(gre->widget),
					  gconf_value_int(value));
		break;
	case GNOP_GNOMENUMBERENTRY:
		gnop_gnome_number_entry_set_long(GNOME_NUMBER_ENTRY(gre->widget),
						 gconf_value_int(value));
		break;
	case GNOP_GNOMECALCULATOR:
		gnome_calculator_set(GNOME_CALCULATOR(gre->widget),
				     gconf_value_int(value));
		break;
	case GNOP_GTKENTRY:
		gnop_gtk_entry_set_long(GTK_ENTRY(gre->widget),
					gconf_value_int(value));
		break;
	case GNOP_GTKCOMBO:
		gnop_gtk_entry_set_long(GTK_ENTRY(GTK_COMBO(gre->widget)->entry),
					gconf_value_int(value));
		break;
	case GNOP_GNOMEENTRY:
		gnop_gnome_entry_set_long(GNOME_ENTRY(gre->widget),
					  gconf_value_int(value));
		break;
	default:
		g_warning(_("Invalid integer widget type"));
		break;
	}
}

static void
gnop_runtime_element_set_float(GnopRuntimeElement *gre, GConfValue *value)
{
	g_return_if_fail(gre != NULL);
	g_return_if_fail(value != NULL);
	g_return_if_fail(value->type == GNOP_TYPE_FLOAT);

	switch(gre->widget_type) {
	case GNOP_GTKSPINBUTTON:
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(gre->widget),
					  gconf_value_float(value));
		break;
	case GNOP_GNOMENUMBERENTRY:
		gnop_gnome_number_entry_set_double(GNOME_NUMBER_ENTRY(gre->widget),
						   gconf_value_float(value));
		break;
	case GNOP_GNOMECALCULATOR:
		gnome_calculator_set(GNOME_CALCULATOR(gre->widget),
				     gconf_value_float(value));
		break;
	case GNOP_GTKENTRY:
		gnop_gtk_entry_set_double(GTK_ENTRY(gre->widget),
					  gconf_value_float(value));
		break;
	case GNOP_GTKCOMBO:
		gnop_gtk_entry_set_double
			(GTK_ENTRY(GTK_COMBO(gre->widget)->entry),
			 gconf_value_float(value));
		break;
	case GNOP_GNOMEENTRY:
		gnop_gnome_entry_set_double(GNOME_ENTRY(gre->widget),
					    gconf_value_float(value));
		break;
	default:
		g_warning(_("Invalid double widget type"));
		break;
	}
}

static void
gnop_runtime_element_set_string(GnopRuntimeElement *gre, GConfValue *value)
{
	g_return_if_fail(gre != NULL);
	g_return_if_fail(value != NULL);
	g_return_if_fail(value->type == GNOP_TYPE_STRING);

	switch(gre->widget_type) {
	case GNOP_GTKENTRY:
		gtk_entry_set_text(GTK_ENTRY(gre->widget),
				   gconf_value_string(value));
		break;
	case GNOP_GTKCOMBO:
		gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(gre->widget)->entry),
				   gconf_value_string(value));
		break;
	case GNOP_GNOMEENTRY:
		gnop_gnome_entry_set_text(GNOME_ENTRY(gre->widget),
					  gconf_value_string(value));
		break;
	case GNOP_GNOMEFILEENTRY:
		gnop_gnome_file_entry_set_text(GNOME_FILE_ENTRY(gre->widget),
					       gconf_value_string(value));
		break;
	case GNOP_GNOMEPIXMAPENTRY:
		gnop_gnome_pixmap_entry_set_text(GNOME_PIXMAP_ENTRY(gre->widget),
						 gconf_value_string(value));
		break;
	case GNOP_GNOMEICONENTRY:
		gnome_icon_entry_set_icon(GNOME_ICON_ENTRY(gre->widget),
					  gconf_value_string(value));
		break;
	default:
		g_warning(_("Invalid string widget type"));
		break;
	}
}

static void
gnop_runtime_element_set_bool(GnopRuntimeElement *gre, GConfValue *value)
{
	g_return_if_fail(gre != NULL);
	g_return_if_fail(value != NULL);
	g_return_if_fail(value->type == GNOP_TYPE_BOOL);

	switch(gre->widget_type) {
	case GNOP_GTKTOGGLEBUTTON:
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(gre->widget),
					     gconf_value_bool(value));
		break;
	default:
		g_warning(_("Invalid boolean widget type"));
		break;
	}
}

static void
gnop_runtime_element_set_weird(GnopRuntimeElement *gre, GConfValue *value)
{
	char *s;

	g_return_if_fail(gre != NULL);
	g_return_if_fail(value != NULL);

	s = gnop_string_from_gconf_value(value);

	switch(gre->widget_type) {
	case GNOP_GTKENTRY:
		gtk_entry_set_text(GTK_ENTRY(gre->widget), s);
		break;
	case GNOP_GTKCOMBO:
		gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(gre->widget)->entry),
				   s);
		break;
	case GNOP_GNOMEENTRY:
		gnop_gnome_entry_set_text(GNOME_ENTRY(gre->widget), s);
		break;
	default:
		g_warning(_("Invalid list/pair widget type"));
		break;
	}
}

void
gnop_runtime_element_set_value(GnopRuntimeElement *gre, GConfValue *value)
{
	GConfValue *old_value;

	g_return_if_fail(gre != NULL);
	g_return_if_fail(value != NULL);

	old_value = gnop_runtime_element_get_value(gre);
	if(gnop_gconf_value_equal(value, old_value)) {
		gconf_value_destroy(old_value);
		return;
	}
	gconf_value_destroy(old_value);

	if(gre->widget_type == GNOP_NATIVEWIDGET) {
		if( ! gnop_widget_interface_set_value(gre->widget, value))
			g_warning(_("Error setting widget value"));
		return;
	}

	/* Non native widgets, only supported types need to be here */
	switch(gre->el->type) {
	case GNOP_TYPE_STRING:
		gnop_runtime_element_set_string(gre, value);
		break;
	case GNOP_TYPE_INT:
		gnop_runtime_element_set_int(gre, value);
		break;
	case GNOP_TYPE_FLOAT:
		gnop_runtime_element_set_float(gre, value);
		break;
	case GNOP_TYPE_BOOL:
		gnop_runtime_element_set_bool(gre, value);
		break;
	default:
		gnop_runtime_element_set_weird(gre, value);
		break;
	}
}

GnopSensitivity	*
gnop_sensitivity_new(GnopElement *ge)
{
	GnopSensitivity *sens = g_new0(GnopSensitivity, 1);

	sens->type = ge->type;
	sens->values = NULL;
	sens->comparison = GNOP_COMP_EQ;
	sens->connection = GNOP_CONNECTION_OR;
	sens->sensitive = NULL;
	sens->insensitive = NULL;

	return sens;
}

void
gnop_sensitivity_destroy(GnopSensitivity *sens)
{
	g_list_foreach(sens->values, (GFunc)gconf_value_destroy, NULL);
	g_list_free(sens->values);
	sens->values = NULL;

	g_slist_foreach(sens->sensitive, (GFunc)g_free, NULL);
	g_slist_free(sens->sensitive);
	sens->sensitive = NULL;

	g_slist_foreach(sens->insensitive, (GFunc)g_free, NULL);
	g_slist_free(sens->insensitive);
	sens->insensitive = NULL;

	g_free(sens);
}

void
gnop_sensitivity_set_connection(GnopSensitivity *sens,
				GnopConnection connection)
{
	g_return_if_fail(sens != NULL);
	g_return_if_fail(connection > GNOP_CONNECTION_INVALID &&
			 connection < GNOP_CONNECTION_LAST);

	sens->connection = connection;
}

void
gnop_sensitivity_set_comparison(GnopSensitivity *sens,
				GnopComparison comparison)
{
	g_return_if_fail(sens != NULL);
	g_return_if_fail(comparison > GNOP_COMP_INVALID &&
			 comparison < GNOP_COMP_LAST);

	sens->comparison = comparison;
}

void
gnop_sensitivity_add_sensitive(GnopSensitivity *sens, const char *widget)
{
	g_return_if_fail(sens != NULL);
	g_return_if_fail(widget != NULL);

	sens->sensitive = g_slist_append(sens->sensitive, g_strdup(widget));
}

void
gnop_sensitivity_add_insensitive(GnopSensitivity *sens, const char *widget)
{
	g_return_if_fail(sens != NULL);
	g_return_if_fail(widget != NULL);

	sens->insensitive = g_slist_append(sens->insensitive, g_strdup(widget));
}

void
gnop_sensitivity_add_value(GnopSensitivity *sens, GConfValue *value)
{
	g_return_if_fail(sens != NULL);
	g_return_if_fail(value != NULL);

	g_return_if_fail(sens->type == gnop_type_from_gconf_value(value));

	sens->values = g_list_prepend(sens->values, gconf_value_copy(value));
}

void
gnop_sensitivity_add_value_from_string(GnopSensitivity *sens,
				       const char *str)
{
	GConfValue *value;

	g_return_if_fail(sens != NULL);
	g_return_if_fail(str != NULL);

	value = gnop_gconf_value_from_string(str, sens->type);
	if (value == NULL) {
		g_warning(_("Unsupported sensitivity value type"));
		return;
	}

	sens->values = g_list_prepend(sens->values, value);
}

void
gnop_sensitivity_remove_value(GnopSensitivity *sens, GConfValue *value)
{
	GList *li;

	g_return_if_fail(sens != NULL);
	g_return_if_fail(value != NULL);

	g_return_if_fail(sens->type == gnop_type_from_gconf_value(value));

	for (li = sens->values; li != NULL; li = li->next) {
		GConfValue *valiter = li->data;
		if (gnop_gconf_value_equal(value, valiter)) {
			sens->values = g_list_remove_link(sens->values, li);
			li->data = NULL;
			g_list_free_1(li);
			gconf_value_destroy(valiter);
			return;
		}
	}
}
