/* GnoP: the pane interface
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "config.h"
#include <gnome.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>
#include <gmodule.h>

#include "gnop.h"
#include "gnopelement.h"
#include "gnopwidgetutil.h"

#include "gnop-ui-internal.h"

#include "gnop-widget-interface.h"
#include "gnop-pane-widget.h"
#include "gnop-native-widgets.h"

#include "gnoppane.h"

/*
 * Type translation
 */
static struct {
	char *name;
	GnopWidgetType type;
} type_table[] = {
	{ "GtkSpinButton", GNOP_GTKSPINBUTTON },
	{ "GnomeNumberEntry", GNOP_GNOMENUMBERENTRY },
	{ "GnomeCalculator", GNOP_GNOMECALCULATOR },
	{ "GtkEntry", GNOP_GTKENTRY },
	{ "GtkCombo", GNOP_GTKCOMBO },
	{ "GnomeEntry", GNOP_GNOMEENTRY },
	{ "GnomeFileEntry", GNOP_GNOMEFILEENTRY },
	{ "GnomePixmapEntry", GNOP_GNOMEPIXMAPENTRY },
	{ "GnomeIconEntry", GNOP_GNOMEICONENTRY },
	{ "GtkToggleButton", GNOP_GTKTOGGLEBUTTON },
	{ "GtkCheckButton", GNOP_GTKCHECKBUTTON },
	{ NULL, 0 }
};

static GHashTable *type_hash = NULL;
static GModule *allsymbols = NULL;

/* Gob functions for types */
static char *
remove_sep(const char *base)
{
	char *p;
	char *s = g_strdup(base);
	while((p=strchr(s, ':')))
		strcpy(p,p+1);
	return s;
}

static char *
replace_sep(const char *base, char r)
{
	char *p;
	char *s = g_strdup(base);
	while((p=strchr(s, ':')))
		*p = r;
	if(*s == r) {
		p = g_strdup(s+1);
		g_free(s);
		return p;
	}
	return s;
}

static void
type_hash_init(void)
{
	int i;

	type_hash = g_hash_table_new(g_str_hash, g_str_equal);

	for(i = 0; type_table[i].name != NULL; i++) {
		g_hash_table_insert(type_hash, type_table[i].name,
				    GINT_TO_POINTER(type_table[i].type));
	}
}

static void
type_from_string(const char *string, GnopType *type, GtkType *gtk_type)
{
	char *typestring;
	char *initfunc;
	guint (*get_type)(void);
	gpointer klass;

	*gtk_type = *type = 0;

	/* make sure we have the native widget classes in gtk type table */
	gnop_lazy_init();

	if( ! type_hash)
		type_hash_init();

	typestring = remove_sep(string);
	/* this will give 0, which is the invalid widget, if we don't
	 * find the string */
	*type = GPOINTER_TO_INT(g_hash_table_lookup(type_hash, typestring));
	if(*type > 0) {
		g_free(typestring);
		return;
	}

	*gtk_type = gtk_type_from_name(typestring);

	g_free(typestring);

	if(*gtk_type == 0) {
		if( ! g_module_supported()) {
			g_warning(_("GModule is not supported and a widget "
				    "of type '%s' has not yet been "
				    "initialized!"));
			return;
		}
		typestring = replace_sep(string, '_');
		initfunc = g_strconcat(typestring, "_get_type", NULL);
		g_free(typestring);
		g_strdown(initfunc);

		if( ! allsymbols)
			allsymbols = g_module_open(NULL, 0);

		if( ! g_module_symbol(allsymbols, initfunc,
				      (gpointer *)&get_type)) {
			g_warning(_("Unknown widget %s found"), string);
			g_free(initfunc);
			return;
		}
		g_free(initfunc);

		g_assert(get_type != NULL);

		*gtk_type = get_type();
	}

	if(*gtk_type == 0) {
		g_warning(_("Unknown widget '%s' found"), string);
		return;
	}

	/* we use gtk_type_name as a checking thing to see if this type
	 * actually exists */
	if( ! gtk_type_name(*gtk_type)) {
		g_warning(_("Strange widget type '%s' found"), string);
		*gtk_type = 0;
		return;
	}
	
	if( ! gtk_type_is_a(*gtk_type, gtk_widget_get_type())) {
		g_warning(_("Non-widget object '%s' found"), string);
		*gtk_type = 0;
		return;
	}

	klass = gtk_type_class(*gtk_type);
	
	if( ! g_dataset_get_data(klass, "gnop_widget_interface")) {
		g_warning(_("Non-compliant widget '%s' found"), string);
		*gtk_type = 0;
		return;
	}

	*type = GNOP_NATIVEWIDGET;
}

/*
 * PaneBase
 */
void
gnop_pane_base_init(GnopPaneBase *base, GDestroyNotify destructor)
{
	static int foo = 1;

	base->refcount = 1;
	base->name = g_strdup_printf("__thingie_%d", foo++);
	base->label = NULL;
	base->levels = NULL;

	base->destructor = destructor;
}

void
gnop_pane_base_ref(GnopPaneBase *base)
{
	g_return_if_fail(base != NULL);

	base->refcount ++;
}

void
gnop_pane_base_unref(GnopPaneBase *base)
{
	g_return_if_fail(base != NULL);

	base->refcount --;

	if(base->refcount == 0) {
		if(base->destructor)
			base->destructor(base);
		base->destructor = NULL;

		g_free(base->label);
		base->label = NULL;

		g_free(base->name);
		base->name = NULL;

		g_strfreev(base->levels);
		base->levels = NULL;

		g_free(base);
	}
}

void
gnop_pane_base_set_name(GnopPaneBase *base, const char *name)
{
	g_return_if_fail(base != NULL);
	g_return_if_fail(name != NULL);

	g_free(base->name);
	base->name = g_strdup(name);
}

void
gnop_pane_base_set_label(GnopPaneBase *base, const char *label)
{
	g_return_if_fail(base != NULL);

	base->label = label ? g_strdup(label) : NULL;
}

void
gnop_pane_base_set_levels(GnopPaneBase *base, const char **levels)
{
	int i;

	g_return_if_fail(base != NULL);

	g_strfreev(base->levels);
	base->levels = NULL;
	if( ! levels)
		return;
	for(i = 0; levels[i]; i++)
		;
	base->levels = g_new0(char *, i+1);
	for(i = 0; levels[i]; i++)
		base->levels[i] = g_strdup(levels[i]);
	base->levels[i] = NULL;
}

void
gnop_pane_base_set_levels_string(GnopPaneBase *base, const char *levels)
{
	g_return_if_fail(base != NULL);

	g_strfreev(base->levels);
	base->levels = NULL;

	if( ! levels)
		return;

	base->levels = g_strsplit(levels, ",", 0);
}

/*
 * Pane
 */
static void
gnop_pane_destructor(gpointer data)
{
	GnopPane *pane = data;

	g_return_if_fail(pane != NULL);

	g_list_foreach(pane->groups, (GFunc)gnop_pane_base_unref, NULL);
	g_list_free(pane->groups);
	pane->groups = NULL;
}

GnopPane *
gnop_pane_new(void)
{
	static int foo = 1;
	GnopPane *pane = g_new0(GnopPane, 1);

	gnop_pane_base_init(&pane->base,
			    gnop_pane_destructor);

	pane->base.label = g_strdup_printf(_("Pane %d"), foo++);
	pane->groups = NULL;

	return pane;
}

void
gnop_pane_append_group(GnopPane *pane, GnopGroup *group)
{
	g_return_if_fail(pane != NULL);
	g_return_if_fail(group != NULL);

	if(g_list_find(pane->groups, group))
		return;

	gnop_pane_base_ref(&group->base);
	pane->groups = g_list_append(pane->groups, group);
}

/*
 * Group
 */
static void
gnop_group_destructor(gpointer data)
{
	GnopGroup *group = data;

	g_return_if_fail(group != NULL);

	g_list_foreach(group->widgets, (GFunc)gnop_pane_base_unref, NULL);
	g_list_free(group->widgets);
	group->widgets = NULL;
}

GnopGroup *
gnop_group_new(void)
{
	static int foo = 1;
	GnopGroup *group = g_new0(GnopGroup, 1);

	gnop_pane_base_init(&group->base,
			    gnop_group_destructor);

	group->base.label = g_strdup_printf(_("Group %d"), foo++);

	group->expand = FALSE;
	group->widgets = NULL;

	return group;
}

void
gnop_group_set_expand(GnopGroup *group, gboolean expand)
{
	g_return_if_fail(group != NULL);

	group->expand = expand;
}

void
gnop_group_append_widget(GnopGroup *group, GnopWidget *widget)
{
	g_return_if_fail(group != NULL);
	g_return_if_fail(widget != NULL);

	if(g_list_find(group->widgets, widget))
		return;

	gnop_pane_base_ref(&widget->base);
	group->widgets = g_list_append(group->widgets, widget);
}

/*
 * Widget
 */
static void
gnop_widget_destructor(gpointer data)
{
	GnopWidget *widget = data;
	GList *li;

	g_return_if_fail(widget != NULL);

	for(li = widget->arguments; li != NULL; li = li->next) {
		GnopArgument *a = li->data;
		g_free(a->name);
		a->name = NULL;
		g_free(a->value);
		a->value = NULL;
		g_free(a);
		li->data = NULL;
	}
	g_list_free(widget->arguments);
	widget->arguments = NULL;

	for(li = widget->options; li != NULL; li = li->next) {
		GnopOption *o = li->data;
		g_free(o->label);
		o->label = NULL;
		g_free(o->value);
		o->value = NULL;
		g_free(o);
	}
	g_list_free(widget->options);
	widget->options = NULL;
}

GnopWidget *
gnop_widget_new(void)
{
	GnopWidget *widget = g_new0(GnopWidget, 1);

	gnop_pane_base_init(&widget->base,
			    gnop_widget_destructor);

	widget->expand = FALSE;
	widget->widget_type = GNOP_INVALIDWIDGET;
	widget->widget_gtktype = 0;
	widget->arguments = NULL;
	widget->options = NULL;

	return widget;
}

void
gnop_widget_set_expand(GnopWidget *widget, gboolean expand)
{
	g_return_if_fail(widget != NULL);

	widget->expand = expand;
}

void
gnop_widget_set_widget_type(GnopWidget *widget, int widget_type)
{
	g_return_if_fail(widget != NULL);
	g_return_if_fail(widget_type >= GNOP_INVALIDWIDGET);
	g_return_if_fail(widget_type < GNOP_LASTWIDGET);

	widget->widget_type = widget_type;
}

void
gnop_widget_set_widget_gtktype(GnopWidget *widget, GtkType widget_gtktype)
{
	g_return_if_fail(widget != NULL);

	widget->widget_gtktype = widget_gtktype;
}

void
gnop_widget_set_widget_type_string(GnopWidget *widget, const char *widget_type)
{
	GnopType type = 0;
	GtkType gtk_type = 0;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(widget_type != NULL);

	type_from_string(widget_type, &type, &gtk_type);

	gnop_widget_set_widget_type(widget, type);
	gnop_widget_set_widget_gtktype(widget, gtk_type);
}

void
gnop_widget_add_argument(GnopWidget *widget,
			 const char *arg_name,
			 const char *arg_value)
{
	GnopArgument *a = g_new0(GnopArgument, 1);
	a->name = g_strdup(arg_name);
	a->value = g_strdup(arg_value);

	widget->arguments = g_list_append(widget->arguments, a);
}

void
gnop_widget_add_option(GnopWidget *widget,
		       const char *opt_label,
		       const char *opt_value)
{
	GnopOption *o;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(opt_value != NULL);

	o = g_new0(GnopOption, 1);
	o->label = g_strdup(opt_label);
	o->value = g_strdup(opt_value);

	widget->options = g_list_append(widget->options, o);
}

void
gnop_widget_set_tooltip(GnopWidget *widget, const char *tooltip)
{
	g_return_if_fail(widget != NULL);

	/* handles NULLs ok */
	g_free(widget->tooltip);
	widget->tooltip = g_strdup(tooltip);
}

/*
 * Dialog
 */

static void
free_widget_runtime(gpointer key, gpointer data, gpointer user_data)
{
	GnopWidgetRuntime *gwr = data;
	if(gwr->wid)
		gtk_widget_unref(gwr->wid);
	gwr->wid = NULL;
	gwr->gnopwid = NULL;
	g_free(gwr);
}

static void
unref_widget(gpointer key, gpointer data, gpointer user_data)
{
	GtkWidget *wid = data;
	gtk_widget_unref(wid);
}

static void
gnop_dialog_destroy(GnopDialog *dialog)
{
	if(dialog->wid)
		gtk_widget_unref(dialog->wid);
	dialog->wid = NULL;

	if(dialog->wid)
		gtk_widget_unref(dialog->pane_wid);
	dialog->pane_wid = NULL;

	if(dialog->tooltips)
		gtk_object_unref(GTK_OBJECT(dialog->tooltips));
	dialog->tooltips = NULL;

	if(dialog->panes) {
		g_list_foreach(dialog->panes, (GFunc)gnop_pane_base_unref, NULL);
		g_list_free(dialog->panes);
	}
	dialog->panes = NULL;

	if(dialog->widgets) {
		g_hash_table_foreach(dialog->widgets, free_widget_runtime, NULL);
		g_hash_table_destroy(dialog->widgets);
	}
	dialog->widgets = NULL;

	if(dialog->gtk_widgets) {
		g_hash_table_foreach(dialog->gtk_widgets, unref_widget, NULL);
		g_hash_table_destroy(dialog->gtk_widgets);
	}
	dialog->gtk_widgets = NULL;

	g_free(dialog);
}

static GtkWidget *
label_widget(GtkWidget *w, const char *label)
{
	GtkWidget *hbox, *l;

	hbox = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
	gtk_widget_show(hbox);

	l = gtk_label_new(label);
	gtk_widget_show(l);
	gtk_box_pack_start(GTK_BOX(hbox), l, FALSE, FALSE, 0);

	gtk_box_pack_start(GTK_BOX(hbox), w, TRUE, TRUE, 0);

	return hbox;
}

static void
add_options_to_combo(GtkCombo *combo, GList *options)
{
	GList *list, *li;

	list = NULL;
	for(li = options; li != NULL; li = li->next) {
		GnopOption *opt = li->data;

		list = g_list_append(list, opt->value);
	}

	gtk_combo_set_popdown_strings(combo, list);
	g_list_free(list);
}

static GtkWidget *
gnop_dialog_make_widget(GnopDialog *dialog, GnopWidget *widget)
{
	GtkWidget *w, *realwid = NULL;
	GnopWidgetRuntime *gwr;

	gwr = g_hash_table_lookup(dialog->widgets, widget->base.name);

	/* this is a name clash, so we ignore this widget */
	if( ! gwr ||
	   gwr->gnopwid != widget)
		return NULL;
	
	switch(widget->widget_type) {
	case GNOP_INVALIDWIDGET:
		g_warning(_("Invalid widget type found while creating dialog"));
		realwid = w = NULL;
		break;

	case GNOP_NATIVEWIDGET:
		realwid = w = gtk_type_new(widget->widget_gtktype);
		if(w == NULL || ! GTK_IS_WIDGET(w)) {
			g_warning(_("Cannot create native widget!"));
			realwid = w = NULL;
			break;
		}
		gtk_widget_show(w);
		if(widget->base.label) {
			if(gnop_widget_interface_implements_set_label(realwid))
				gnop_widget_interface_set_label(realwid, widget->base.label);
			else
				w = label_widget(w, widget->base.label);
		}
		if(widget->options &&
		   gnop_widget_interface_implements_add_options(realwid))
			gnop_widget_interface_add_options(realwid, widget->options);
		break;

	case GNOP_GTKSPINBUTTON:
		realwid = w = gtk_spin_button_new(NULL, 1, 0);
		gtk_widget_show(w);
		w = label_widget(w, widget->base.label);
		break;
	case GNOP_GTKENTRY:
		realwid = w = gtk_entry_new();
		gtk_widget_show(w);
		w = label_widget(w, widget->base.label);
		break;
	case GNOP_GTKCOMBO:
		realwid = w = gtk_combo_new();
		add_options_to_combo(GTK_COMBO(w), widget->options);
		gtk_widget_show(w);
		w = label_widget(w, widget->base.label);
		break;
	case GNOP_GNOMEENTRY:
		realwid = w = gnome_entry_new(widget->base.name);
		gtk_widget_show(w);
		w = label_widget(w, widget->base.label);
		break;
	case GNOP_GNOMEFILEENTRY:
		realwid = w = gnome_file_entry_new(widget->base.name,
						   _("Browse"));
		gtk_widget_show(w);
		w = label_widget(w, widget->base.label);
		break;
	case GNOP_GNOMEPIXMAPENTRY:
		realwid = w = gnome_pixmap_entry_new(widget->base.name,
						     _("Browse"), TRUE);
		gtk_widget_show(w);
		w = label_widget(w, widget->base.label);
		break;
	case GNOP_GNOMEICONENTRY:
		realwid = w = gnome_icon_entry_new(widget->base.name,
						   _("Browse"));
		gtk_widget_show(w);
		w = label_widget(w, widget->base.label);
		break;
	case GNOP_GNOMENUMBERENTRY:
		realwid = w = gnome_icon_entry_new(widget->base.name,
						   _("Calculator"));
		gtk_widget_show(w);
		w = label_widget(w, widget->base.label);
		break;
	case GNOP_GTKTOGGLEBUTTON:
		realwid = w =
			gtk_toggle_button_new_with_label(widget->base.label);
		break;
	case GNOP_GTKCHECKBUTTON:
		realwid = w =
			gtk_check_button_new_with_label(widget->base.label);
		break;
	case GNOP_GNOMECALCULATOR:
		realwid = w = gnome_calculator_new();
		break;
	default:
		g_warning(_("Unsupported widget type found while creating dialog"));
		realwid = w = NULL;
		break;
	}
	
	if( ! w)
		return NULL;

	gtk_widget_show(w);

	gwr->wid = realwid;
	gtk_widget_ref(realwid);

	if(widget->arguments) {
		GList *li;
		for(li = widget->arguments; li != NULL; li = li->next) {
			GnopArgument *a = li->data;
			gnop_set_object_argument(GTK_OBJECT(realwid),
						 a->name, a->value);
		}
	}

	if(widget->tooltip) {
		if(GTK_WIDGET_NO_WINDOW(w)) {
			GtkWidget *tmp = w;
			w = gtk_event_box_new();
			gtk_widget_show(w);
			gtk_container_add(GTK_CONTAINER(w), tmp);
			gtk_widget_show(tmp);
		}

		gtk_tooltips_set_tip(dialog->tooltips, w, widget->tooltip, NULL);
	}

	return w;
}

static gboolean
is_in_level(char **levels, const char *level)
{
	int i;

	if( ! levels ||
	    ! level)
		return TRUE;

	for(i = 0; levels[i]; i++) {
		if(strcmp(levels[i], level) == 0)
			return TRUE;
	}
	return FALSE;
}

static GtkWidget *
gnop_dialog_make_group(GnopDialog *dialog, GnopGroup *group, const char *level)
{
	GtkWidget *vbox;
	GList *li;

	vbox = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), GNOME_PAD_SMALL);
	gtk_widget_show(vbox);

	for(li = group->widgets; li != NULL; li = li->next) {
		GnopWidget *widget = li->data;
		GtkWidget *w;

		if( ! is_in_level(widget->base.levels, level))
			continue;

		w = gnop_dialog_make_widget(dialog, widget);

		if(w)
			gtk_box_pack_start(GTK_BOX(vbox), w,
					   widget->expand, widget->expand, 0);

	}

	return vbox;
}

static GtkWidget *
gnop_dialog_make_pane(GnopDialog *dialog, GnopPane *pane, const char *level)
{
	GtkWidget *vbox;
	GList *li;

	vbox = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), GNOME_PAD_SMALL);
	gtk_widget_show(vbox);

	for(li = pane->groups; li != NULL; li = li->next) {
		GnopGroup *group = li->data;
		GtkWidget *w, *frame;

		if( ! is_in_level(group->base.levels, level))
			continue;

		frame = gtk_frame_new(group->base.label);
		gtk_widget_show(frame);

		w = gnop_dialog_make_group(dialog, group, level);
		gtk_container_add(GTK_CONTAINER(frame), w);

		if( ! g_hash_table_lookup(dialog->gtk_widgets,
					  group->base.name)) {
			gtk_widget_ref(w);
			g_hash_table_insert(dialog->gtk_widgets,
					    group->base.name, w);
		}

		gtk_box_pack_start(GTK_BOX(vbox), frame,
				   group->expand, group->expand, 0);

	}

	return vbox;
}

static void
insert_widget(GnopDialog *dialog, GHashTable *gtk_widgets, GnopWidget *widget)
{
	GnopWidgetRuntime *gwr;

	if(g_hash_table_lookup(dialog->widgets, widget->base.name)) {
		g_warning(_("Widget of name '%s' is already added!"),
			  widget->base.name);
		return;
	}

	if(g_hash_table_lookup(gtk_widgets, widget->base.name)) {
		g_warning(_("Widget of name '%s' is already added!"),
			  widget->base.name);
		return;
	}

	gwr = g_new0(GnopWidgetRuntime, 1);
	gwr->wid = NULL;
	gwr->gnopwid = widget;
	g_hash_table_insert(dialog->widgets, widget->base.name, gwr);
}

static void
insert_gtk_widget(GnopDialog *dialog, GHashTable *gtk_widgets, char *name)
{
	if(g_hash_table_lookup(dialog->widgets, name)) {
		g_warning(_("Widget of name '%s' is already added!"), name);
		return;
	}

	if(g_hash_table_lookup(gtk_widgets, name)) {
		g_warning(_("Widget of name '%s' is already added!"), name);
		return;
	}

	g_hash_table_insert(gtk_widgets, name, GINT_TO_POINTER(1));
}

static void
dialog_setup_hash(GnopDialog *dialog, GList *panes)
{
	GList *lip, *lig, *liw;
	GHashTable *gtk_widgets;

	/* temporary hash table just to find duplicates */
	gtk_widgets = g_hash_table_new(g_str_hash, g_str_equal);

	for(lip = panes; lip != NULL; lip = lip->next) {
		GnopPane *pane = lip->data;
		insert_gtk_widget(dialog, gtk_widgets,
				  pane->base.name);
		for(lig = pane->groups; lig != NULL; lig = lig->next) {
			GnopGroup *group = lig->data;
			insert_gtk_widget(dialog, gtk_widgets,
					  group->base.name);
			for(liw = group->widgets; liw != NULL;
			    liw = liw->next) {
				GnopWidget *widget = liw->data;
				insert_widget(dialog, gtk_widgets, widget);
			}
		}
	}

	g_hash_table_destroy(gtk_widgets);
}

static void
dialog_setup_unknowns(GnopElement *element, gpointer data)
{
	GnopDialog *dialog = data;
	GnopWidget *widget = NULL;

	if(element->widget)
		widget = gnop_dialog_lookup_widget(dialog, element->widget);
	if( ! widget)
		widget = gnop_dialog_lookup_widget(dialog, element->conf_path);
	if( ! widget)
		return;

	if(widget->widget_type == GNOP_INVALIDWIDGET) {
		switch(element->type) {
		case GNOP_TYPE_BOOL:
			widget->widget_type = GNOP_GTKCHECKBUTTON;
			break;
		/* everything else defaults to an entry for now */
		default:
			widget->widget_type = GNOP_GTKENTRY;
			break;
		}
	}
}

static void
dialog_clicked(GtkWidget *dialog, int button, gpointer data)
{
	GnopUIInternal *ui = data;

	if(button == 0) {
		gnop_ui_show_help(GNOP_UI(ui));
	} else {
		gnome_dialog_close(GNOME_DIALOG(dialog));
	}


}

static void
dialog_destroyed(GtkWidget *dialog, gpointer data)
{
	GnopUIInternal *ui = data;
	gtk_object_unref(GTK_OBJECT(ui));
}

GnopDialog *
gnop_dialog_new(GnopUIInternal *ui, GnopFile *file, const char *level)
{
	GnopDialog *dialog;
	GList *li;

	g_return_val_if_fail(ui != NULL, NULL);
	g_return_val_if_fail(GNOP_IS_UI_INTERNAL(ui), NULL);
	g_return_val_if_fail(file != NULL, NULL);

	if( ! file->panes) {
		g_warning(_("No file or panes to make dialog from"));
		return NULL;
	}
       
	dialog = g_new0(GnopDialog, 1);

	dialog->tooltips = gtk_tooltips_new();

	dialog->refcount = 1;
	if(file->help_name)
		dialog->wid = gnome_dialog_new(file->dialog_title, 
					       GNOME_STOCK_BUTTON_HELP,
					       GNOME_STOCK_BUTTON_CLOSE,
					       NULL);
	else
		dialog->wid = gnome_dialog_new(file->dialog_title, 
					       GNOME_STOCK_BUTTON_CLOSE,
					       NULL);
	if(file->help_name) {
		gtk_object_ref(GTK_OBJECT(ui));
		gtk_signal_connect(GTK_OBJECT(dialog->wid), "clicked",
				   GTK_SIGNAL_FUNC(dialog_clicked),
				   ui);
		gtk_signal_connect(GTK_OBJECT(dialog->wid), "destroy",
				   GTK_SIGNAL_FUNC(dialog_destroyed),
				   ui);
	} else
		gnome_dialog_set_close(GNOME_DIALOG(dialog->wid), TRUE);
	gtk_widget_ref(dialog->wid);
	dialog->panes = g_list_copy(file->panes);
	g_list_foreach(dialog->panes, (GFunc)gnop_pane_base_ref, NULL);
	dialog->widgets = g_hash_table_new(g_str_hash, g_str_equal);
	dialog->gtk_widgets = g_hash_table_new(g_str_hash, g_str_equal);

	/* setup our name to gwr hash */
	dialog_setup_hash(dialog, dialog->panes);

	/* setup any invalid widgets (unknowns) to be defaults for
	 * that particular type */
	gnop_file_foreach_element(file, dialog_setup_unknowns, dialog);

	/*FIXME: somehow read preferences and make it possible to change
	 * the style (with set_pane_mode) */
	dialog->pane_wid = gnop_pane_widget_new();
	gtk_widget_ref(dialog->pane_wid);
	gtk_widget_show(dialog->pane_wid);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog->wid)->vbox),
			   dialog->pane_wid, TRUE, TRUE, 0);

	for(li = dialog->panes; li != NULL; li = li->next) {
		GnopPane *pane = li->data;
		GtkWidget *w;

		if( ! is_in_level(pane->base.levels, level))
			continue;

		w = gnop_dialog_make_pane(dialog, pane, level);

		if( ! g_hash_table_lookup(dialog->gtk_widgets,
					  pane->base.name)) {
			gtk_widget_ref(w);
			g_hash_table_insert(dialog->gtk_widgets,
					    pane->base.name, w);
		}
		gnop_pane_widget_append_page(GNOP_PANE_WIDGET(dialog->pane_wid),
					     w, pane->base.label);
	}

	gtk_widget_show(dialog->wid);

	return dialog;
}

void
gnop_dialog_ref(GnopDialog *dialog)
{
	g_return_if_fail(dialog != NULL);

	dialog->refcount ++;
}

void
gnop_dialog_unref(GnopDialog *dialog)
{
	g_return_if_fail(dialog != NULL);

	dialog->refcount --;

	if(dialog->refcount == 0)
		gnop_dialog_destroy(dialog);
}

GnopWidget *
gnop_dialog_lookup_widget(GnopDialog *dialog, const char *name)
{
	GnopWidgetRuntime *gwr;

	g_return_val_if_fail(dialog != NULL, NULL);
	g_return_val_if_fail(name != NULL, NULL);

	gwr = g_hash_table_lookup(dialog->widgets, name);
	if(gwr)
		return gwr->gnopwid;
	else
		return NULL;
}

GtkWidget *
gnop_dialog_lookup_gtk_widget(GnopDialog *dialog, const char *name)
{
	GnopWidgetRuntime *gwr;

	g_return_val_if_fail(dialog != NULL, NULL);
	g_return_val_if_fail(name != NULL, NULL);

	gwr = g_hash_table_lookup(dialog->widgets, name);
	if(gwr)
		return gwr->wid;
	else
		return g_hash_table_lookup(dialog->gtk_widgets, name);
}

void
gnop_pane_free_unused_data(void)
{
	if(type_hash) {
		g_hash_table_destroy(type_hash);
		type_hash = NULL;
	}
	if(allsymbols)
		g_module_close(allsymbols);
}

