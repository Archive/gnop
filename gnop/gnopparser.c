/* GnoP: the parser using SAX interface
 * Author: George Lebl
 * (c) 1999 Free Software Foundation
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "config.h"
#include <gnome.h>
#include <gnome-xml/parser.h>
#include <gnome-xml/parserInternals.h>

#include "gnoppane.h"
#include "gnopelement.h"

#include "gnopparser.h"

extern GList *gnop_directories;

enum {
	IGNORE,
	START,
	FINISH,
	LOOKING_FOR_ELEMENT,

	INSIDE_ELEMENT,

	INSIDE_BASECONFPATH,
	INSIDE_DIALOG_TITLE,
	INSIDE_HELP,
	INSIDE_HELP_NAME,
	INSIDE_HELP_PATH,
	INSIDE_LEVEL,
	INSIDE_LEVEL_NAME,
	INSIDE_LEVEL_BASECONFPATH,

	INSIDE_ELEMENT_TYPE,
	INSIDE_ELEMENT_CONF_PATH,
	INSIDE_ELEMENT_WIDGET,

	INSIDE_SENSITIVITY,
	INSIDE_SENSITIVITY_CONNECTION,
	INSIDE_SENSITIVITY_VALUE,
	INSIDE_SENSITIVITY_COMPARISON,
	INSIDE_SENSITIVITY_SENSITIVE,
	INSIDE_SENSITIVITY_INSENSITIVE,

	INSIDE_PANE,
	INSIDE_PANE_NAME,
	INSIDE_PANE_LABEL,
	INSIDE_PANE_LEVELS,

	INSIDE_GROUP,
	INSIDE_GROUP_NAME,
	INSIDE_GROUP_LABEL,
	INSIDE_GROUP_LEVELS,
	INSIDE_GROUP_EXPAND,

	INSIDE_WIDGET,
	INSIDE_WIDGET_NAME,
	INSIDE_WIDGET_LABEL,
	INSIDE_WIDGET_LEVELS,
	INSIDE_WIDGET_EXPAND,
	INSIDE_WIDGET_TYPE,
	INSIDE_WIDGET_TOOLTIP,

	INSIDE_ARGUMENT,
	INSIDE_ARGUMENT_NAME,
	INSIDE_ARGUMENT_VALUE,
	INSIDE_ARGUMENT_TRANSLATABLE,

	INSIDE_OPTION,
	INSIDE_OPTION_LABEL,
	INSIDE_OPTION_VALUE
};


typedef struct _State State;
struct _State {
	/* stuff that is read */
	GnopFile *gf;
	GnopSensitivity *sens;

	int state;
	int ignore_last_state; /* state to go into after ignore */
	int ignore_depth; /* how deep are we into ingore */
	GString *buf;
	gboolean reading_characters;
	GnopElement *element;

	GnopPane *pane;
	GnopGroup *group;
	GnopWidget *widget;

	char *arg_name;
	char *arg_value;
	gboolean arg_translatable;

	char *opt_label;
	char *opt_value;

	GnopLevel *level;
};

static void startDocument(void *user_data);
static void endDocument(void *user_data);
static void characters(void *user_data, const CHAR *ch, int len);
static void startElement(void *user_data, const CHAR *name, const CHAR **attrs);
static void endElement(void *user_data, const CHAR *name);

static int myXmlSAXParseFile(xmlSAXHandlerPtr sax, void *user_data, const char *filename);
static int myXmlSAXParseMemory(xmlSAXHandlerPtr sax, void *user_data, const char *buffer, int size);

static void gnop_file_add_element(GnopFile *gf, GnopElement *ge);
static void gnop_file_destroy(GnopFile *gf);

static void unref_elements(gpointer key, gpointer data, gpointer user_data);
static gboolean kill_unused_gnopfiles(gpointer key, gpointer data, gpointer user_data);
static void add_elements_to_list(gpointer key, gpointer data, gpointer user_data);
static void foreach_elements(gpointer key, gpointer data, gpointer user_data);

static GnopComparison get_comparison(const char *name);

static xmlSAXHandler handler = {0};
static GHashTable *file_hash = NULL;
static gboolean parser_inited = FALSE;

static GHashTable *comparison_hash = NULL;

static struct {
	char *name;
	int value;
} comparisons[] = {
	{"LT", GNOP_COMP_LT},
	{"LessThen", GNOP_COMP_LT},
	{"Less Then", GNOP_COMP_LT},
	{"<", GNOP_COMP_LT},
	{"LE", GNOP_COMP_LE},
	{"LessThenOrEqual", GNOP_COMP_LE},
	{"Less Then Or Equal", GNOP_COMP_LE},
	{"LessThenOrEqualTo", GNOP_COMP_LE},
	{"Less Then Or Equal To", GNOP_COMP_LE},
	{"<=", GNOP_COMP_LE},
	{"EQ", GNOP_COMP_EQ},
	{"Equal", GNOP_COMP_EQ},
	{"Equals", GNOP_COMP_EQ},
	{"=", GNOP_COMP_EQ},
	{"==", GNOP_COMP_EQ},
	{"NE", GNOP_COMP_NE},
	{"NotEqual", GNOP_COMP_NE},
	{"Not Equal", GNOP_COMP_NE},
	{"Unequal", GNOP_COMP_NE},
	{"!=", GNOP_COMP_NE},
	{"<>", GNOP_COMP_NE},
	{"GE", GNOP_COMP_GE},
	{"GreaterThenOrEqual", GNOP_COMP_GE},
	{"Greater Then Or Equal", GNOP_COMP_GE},
	{"GreaterThenOrEqualTo", GNOP_COMP_GE},
	{"Greater Then Or Equal To", GNOP_COMP_GE},
	{">=", GNOP_COMP_GE},
	{"GT", GNOP_COMP_GT},
	{"GreaterThen", GNOP_COMP_GT},
	{"Greater Then", GNOP_COMP_GT},
	{">", GNOP_COMP_GT},
	{NULL, 0}
};

static void
init_comparison_hash(void)
{
	int i;
	if(comparison_hash != NULL)
		return;

	comparison_hash = g_hash_table_new(g_str_hash, g_str_equal);

	for (i = 0; comparisons[i].name != NULL; i++) {
		g_hash_table_insert(comparison_hash, comparisons[i].name,
				    GINT_TO_POINTER(comparisons[i].value));
	}
}

static GnopComparison
get_comparison(const char *name)
{
	g_return_val_if_fail(name != NULL, GNOP_COMP_INVALID);

	if(comparison_hash == NULL)
		init_comparison_hash();

	/* INVALID == 0, so if it can't find it it will
	 * return INVALID */
	return GPOINTER_TO_INT(g_hash_table_lookup(comparison_hash, name));
}

static GnopConnection
get_connection(const char *name)
{
	g_return_val_if_fail(name != NULL, GNOP_CONNECTION_INVALID);

	if (g_strcasecmp(name, "OR") == 0)
		return GNOP_CONNECTION_OR;
	else if (g_strcasecmp(name, "AND") == 0)
		return GNOP_CONNECTION_AND;
	else
		return GNOP_CONNECTION_INVALID;
}


static void
parser_init(void)
{
	if(parser_inited) return;

	handler.startDocument = startDocument;
	handler.endDocument = endDocument;
	handler.characters = characters;
	handler.startElement = startElement;
	handler.endElement = endElement;

	file_hash = g_hash_table_new(g_str_hash, g_str_equal);

	parser_inited = TRUE;
}

static void
startDocument(void *user_data)
{
	State *state = user_data;

	state->state = START;

	state->buf = g_string_new("");
	state->reading_characters = FALSE;
}

static void
endDocument(void *user_data)
{
	State *state = user_data;

	if(state->sens) {
		gnop_sensitivity_destroy(state->sens);
		state->sens = NULL;
	}

	g_string_free(state->buf, TRUE);
	state->buf = NULL;
	state->reading_characters = FALSE;
}

static void
characters(void *user_data, const CHAR *ch, int len)
{
	State *state = user_data;
	char *s;

	if(!state->reading_characters)
		return;

	/* there must be a better way of doing this */
	s = g_strndup(ch, len);
	g_string_append(state->buf, s);
	g_free(s);
}

#define START_READING_CHARS(element) 		\
	state->state = element;			\
						\
	/* clear buffer for attribute */	\
	g_string_assign(state->buf, "");	\
						\
	/* read characters now */		\
	state->reading_characters = TRUE;

#define START_IGNORE 					\
	state->ignore_last_state = state->state;	\
	state->state = IGNORE;				\
	state->ignore_depth = 1;

static void
startElement(void *user_data, const CHAR *name, const CHAR **attrs)
{
	State *state = user_data;

	switch(state->state) {
	case IGNORE:
		state->ignore_depth++;
		break;

	case START:
		if(g_strcasecmp(name, "GnopElements")==0) {
			state->state = LOOKING_FOR_ELEMENT;
		} else {
			START_IGNORE;
		}
		break;

	case FINISH: /* just ingore everything now */
		break;

	case LOOKING_FOR_ELEMENT:
		if(g_strcasecmp(name, "Element")==0) {
			state->state = INSIDE_ELEMENT;
			state->element = gnop_element_new();
		} else if(g_strcasecmp(name, "Pane")==0) {
			state->state = INSIDE_PANE;
			state->pane = gnop_pane_new();
		} else if(g_strcasecmp(name, "BaseConfPath")==0) {
			START_READING_CHARS(INSIDE_BASECONFPATH)
		} else if(g_strcasecmp(name, "DialogTitle")==0) {
			START_READING_CHARS(INSIDE_DIALOG_TITLE)
		} else if(g_strcasecmp(name, "Help")==0) {
			state->state = INSIDE_HELP;
			g_free(state->gf->help_name);
			state->gf->help_name = g_strdup("gnop");
			g_free(state->gf->help_path);
			state->gf->help_path = g_strdup("generic.html");
		} else if(g_strcasecmp(name, "Level")==0) {
			state->level = g_new0(GnopLevel, 1);
			state->state = INSIDE_LEVEL;
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_HELP:
		if(g_strcasecmp(name, "Name")==0) {
			START_READING_CHARS(INSIDE_HELP_NAME)
		} else if(g_strcasecmp(name, "Path")==0) {
			START_READING_CHARS(INSIDE_HELP_PATH)
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_LEVEL:
		if(g_strcasecmp(name, "Name")==0) {
			START_READING_CHARS(INSIDE_LEVEL_NAME)
		} else if(g_strcasecmp(name, "BaseConfPath")==0) {
			START_READING_CHARS(INSIDE_LEVEL_BASECONFPATH)
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_ELEMENT:
		if(g_strcasecmp(name, "Type") == 0) {
			START_READING_CHARS(INSIDE_ELEMENT_TYPE);
		} else if(g_strcasecmp(name, "ConfPath") == 0) {
			START_READING_CHARS(INSIDE_ELEMENT_CONF_PATH);
		} else if(g_strcasecmp(name, "Widget") == 0) {
			START_READING_CHARS(INSIDE_ELEMENT_WIDGET);
		} else if(g_strcasecmp(name, "Sensitivity") == 0) {
			if(state->element->type == GNOP_TYPE_INVALID) {
				g_warning(_("Type not set before Sensitivity"));
				state->ignore_last_state = state->state;
				state->state = IGNORE;
				state->ignore_depth = 1;
			} else  {
				state->state = INSIDE_SENSITIVITY;
				state->sens =
					gnop_sensitivity_new(state->element);
			}
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_SENSITIVITY:
		if(g_strcasecmp(name, "Value") == 0) {
			START_READING_CHARS(INSIDE_SENSITIVITY_VALUE);
		} else if(g_strcasecmp(name, "Connection") == 0) {
			START_READING_CHARS(INSIDE_SENSITIVITY_CONNECTION);
		} else if(g_strcasecmp(name, "Comparison") == 0) {
			START_READING_CHARS(INSIDE_SENSITIVITY_COMPARISON);
		} else if(g_strcasecmp(name, "Sensitive") == 0) {
			START_READING_CHARS(INSIDE_SENSITIVITY_SENSITIVE);
		} else if(g_strcasecmp(name, "Insensitive") == 0) {
			START_READING_CHARS(INSIDE_SENSITIVITY_INSENSITIVE);
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_PANE:
		if(g_strcasecmp(name, "Group")==0) {
			state->state = INSIDE_GROUP;
			state->group = gnop_group_new();
		} else if(g_strcasecmp(name, "Name")==0) {
			START_READING_CHARS(INSIDE_PANE_NAME);
		} else if(g_strcasecmp(name, "Label")==0) {
			START_READING_CHARS(INSIDE_PANE_LABEL);
		} else if(g_strcasecmp(name, "Levels")==0) {
			START_READING_CHARS(INSIDE_PANE_LEVELS);
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_GROUP:
		if(g_strcasecmp(name, "Widget")==0) {
			state->state = INSIDE_WIDGET;
			state->widget = gnop_widget_new();
		} else if(g_strcasecmp(name, "Name")==0) {
			START_READING_CHARS(INSIDE_GROUP_NAME);
		} else if(g_strcasecmp(name, "Label")==0) {
			START_READING_CHARS(INSIDE_GROUP_LABEL);
		} else if(g_strcasecmp(name, "Levels")==0) {
			START_READING_CHARS(INSIDE_GROUP_LEVELS);
		} else if(g_strcasecmp(name, "Expand")==0) {
			START_READING_CHARS(INSIDE_GROUP_EXPAND);
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_WIDGET:
		if(g_strcasecmp(name, "Argument")==0) {
			state->state = INSIDE_ARGUMENT;
			state->arg_name = NULL;
			state->arg_value = NULL;
			state->arg_translatable = FALSE;
		} else if(g_strcasecmp(name, "Option")==0) {
			state->state = INSIDE_OPTION;
			state->opt_label = NULL;
			state->opt_value = NULL;
		} else if(g_strcasecmp(name, "Name")==0) {
			START_READING_CHARS(INSIDE_WIDGET_NAME);
		} else if(g_strcasecmp(name, "Label")==0) {
			START_READING_CHARS(INSIDE_WIDGET_LABEL);
		} else if(g_strcasecmp(name, "Levels")==0) {
			START_READING_CHARS(INSIDE_WIDGET_LEVELS);
		} else if(g_strcasecmp(name, "Expand")==0) {
			START_READING_CHARS(INSIDE_WIDGET_EXPAND);
		} else if(g_strcasecmp(name, "Type")==0) {
			START_READING_CHARS(INSIDE_WIDGET_TYPE);
		} else if(g_strcasecmp(name, "Tooltip")==0) {
			START_READING_CHARS(INSIDE_WIDGET_TOOLTIP);
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_ARGUMENT:
		if(g_strcasecmp(name, "Name")==0) {
			START_READING_CHARS(INSIDE_ARGUMENT_NAME);
		} else if(g_strcasecmp(name, "Value")==0) {
			START_READING_CHARS(INSIDE_ARGUMENT_VALUE);
		} else if(g_strcasecmp(name, "Translatable")==0) {
			START_READING_CHARS(INSIDE_ARGUMENT_TRANSLATABLE);
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_OPTION:
		if(g_strcasecmp(name, "Label")==0) {
			START_READING_CHARS(INSIDE_OPTION_LABEL);
		} else if(g_strcasecmp(name, "Value")==0) {
			START_READING_CHARS(INSIDE_OPTION_VALUE);
		} else {
			START_IGNORE;
		}
		break;

	default:
		START_IGNORE;
		break;
	}
}

static void
endElement(void *user_data, const CHAR *name)
{
	State *state = user_data;
	int i, len;

	switch(state->state) {
	case IGNORE:
		state->ignore_depth--;
		if(state->ignore_depth == 0)
			state->state = state->ignore_last_state;
		break;

	case FINISH: /* just ingore everything now */
		break;

	case LOOKING_FOR_ELEMENT:
		state->state = FINISH;
		break;

	case INSIDE_ELEMENT:
		state->state = LOOKING_FOR_ELEMENT;
		if(state->element->type == GNOP_TYPE_INVALID) {
			g_warning(_("Untyped Gnop Element found"));
			gnop_element_unref(state->element);
			state->element = NULL;
			break;
		}
		if( ! state->element->conf_path) {
			g_warning(_("Unnamed Gnop Element found"));
			gnop_element_unref(state->element);
			state->element = NULL;
			break;
		}
		gnop_file_add_element(state->gf, state->element);
		state->element = NULL;
		break;

	case INSIDE_BASECONFPATH:
		state->state = LOOKING_FOR_ELEMENT;
		g_free(state->gf->base_conf_path);
		state->gf->base_conf_path = g_strdup(state->buf->str);

		/* chop off trailing '/' */
		len = strlen(state->gf->base_conf_path);
		if(state->gf->base_conf_path[len-1] == '/')
			state->gf->base_conf_path[len-1] = '\0';

		/*FIXME: do further checking on path (gconf_valid_key??) */

		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_DIALOG_TITLE:
		state->state = LOOKING_FOR_ELEMENT;
		g_free(state->gf->dialog_title);
		state->gf->dialog_title = g_strdup(_(state->buf->str));
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_HELP_NAME:
		state->state = INSIDE_HELP;
		g_free(state->gf->help_name);
		state->gf->help_name = g_strdup(state->buf->str);
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_HELP_PATH:
		state->state = INSIDE_HELP;
		g_free(state->gf->help_path);
		state->gf->help_path = g_strdup(_(state->buf->str));
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_HELP:
		state->state = LOOKING_FOR_ELEMENT;
		break;

	case INSIDE_LEVEL_NAME:
		state->state = INSIDE_LEVEL;
		g_free(state->level->name);
		state->level->name = g_strdup(state->buf->str);
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_LEVEL_BASECONFPATH:
		state->state = INSIDE_LEVEL;
		g_free(state->level->base_conf_path);
		state->level->base_conf_path = g_strdup(state->buf->str);
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_LEVEL:
		if(state->level->name && state->level->base_conf_path) {
			state->gf->levels = g_list_prepend(state->gf->levels, state->level);
		} else {
			g_warning(_("Fields missing from Level tag"));
			g_free(state->level->name);
			g_free(state->level->base_conf_path);
			g_free(state->level);
		}
		state->level = NULL;
		state->state = LOOKING_FOR_ELEMENT;
		break;

	case INSIDE_ELEMENT_TYPE:
		gnop_element_set_type_string(state->element,
					     state->buf->str);

		/* stop reading characters */
		state->reading_characters = FALSE;

		state->state = INSIDE_ELEMENT;
		break;

	case INSIDE_ELEMENT_CONF_PATH:
		gnop_element_set_conf_path(state->element,
					   state->buf->str);

		/* stop reading characters */
		state->reading_characters = FALSE;

		state->state = INSIDE_ELEMENT;
		break;

	case INSIDE_ELEMENT_WIDGET:
		gnop_element_set_widget(state->element,
					state->buf->str);

		/* stop reading characters */
		state->reading_characters = FALSE;

		state->state = INSIDE_ELEMENT;
		break;

	case INSIDE_SENSITIVITY:
		state->state = INSIDE_ELEMENT;
		gnop_element_add_sensitivity(state->element, state->sens);
		state->sens = NULL;
		break;

	case INSIDE_SENSITIVITY_VALUE:
		gnop_sensitivity_add_value_from_string(state->sens, 
						       state->buf->str);
		state->state = INSIDE_SENSITIVITY;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_SENSITIVITY_CONNECTION:
		i = get_connection(state->buf->str);
		if(i == GNOP_CONNECTION_INVALID) {
			g_warning(_("Unknown connection '%s' assuming OR"),
				  state->buf->str);
			i = GNOP_CONNECTION_OR;
		}
		gnop_sensitivity_set_connection(state->sens, i);
		state->state = INSIDE_SENSITIVITY;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_SENSITIVITY_COMPARISON:
		i = get_comparison(state->buf->str);
		if(i == GNOP_COMP_INVALID) {
			g_warning(_("Unknown comparison '%s' assuming equals"),
				  state->buf->str);
			i = GNOP_COMP_EQ;
		}
		gnop_sensitivity_set_comparison(state->sens, i);
		state->state = INSIDE_SENSITIVITY;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_SENSITIVITY_SENSITIVE:
		gnop_sensitivity_add_sensitive(state->sens, state->buf->str);
		state->state = INSIDE_SENSITIVITY;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_SENSITIVITY_INSENSITIVE:
		gnop_sensitivity_add_insensitive(state->sens, state->buf->str);
		state->state = INSIDE_SENSITIVITY;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_PANE:
		state->gf->panes =
			g_list_append(state->gf->panes, state->pane);
		state->pane = NULL;
		state->state = LOOKING_FOR_ELEMENT;
		break;

	case INSIDE_PANE_NAME:
		gnop_pane_base_set_name(&state->pane->base, state->buf->str);
		state->state = INSIDE_PANE;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_PANE_LABEL:
		gnop_pane_base_set_label(&state->pane->base,
					 _(state->buf->str));
		state->state = INSIDE_PANE;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_PANE_LEVELS:
		gnop_pane_base_set_levels_string(&state->pane->base,
						 state->buf->str);
		state->state = INSIDE_PANE;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_GROUP:
		gnop_pane_append_group(state->pane, state->group);
		gnop_pane_base_unref(&state->group->base);
		state->group = NULL;
		state->state = INSIDE_PANE;
		break;

	case INSIDE_GROUP_NAME:
		gnop_pane_base_set_name(&state->group->base, state->buf->str);
		state->state = INSIDE_GROUP;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_GROUP_LABEL:
		gnop_pane_base_set_label(&state->group->base,
					 _(state->buf->str));
		state->state = INSIDE_GROUP;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_GROUP_LEVELS:
		gnop_pane_base_set_levels_string(&state->group->base,
						 state->buf->str);
		state->state = INSIDE_GROUP;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_GROUP_EXPAND:
		if(state->buf->str[0] == 't' || state->buf->str[0] == 'T' ||
		   state->buf->str[0] == 'y' || state->buf->str[0] == 'Y' ||
		   atoi(state->buf->str) != 0)
			gnop_group_set_expand(state->group, TRUE);
		else
			gnop_group_set_expand(state->group, FALSE);
		state->state = INSIDE_WIDGET;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_WIDGET:
		gnop_group_append_widget(state->group, state->widget);
		gnop_pane_base_unref(&state->widget->base);
		state->widget = NULL;
		state->state = INSIDE_GROUP;
		break;

	case INSIDE_WIDGET_NAME:
		gnop_pane_base_set_name(&state->widget->base, state->buf->str);
		state->state = INSIDE_WIDGET;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_WIDGET_TOOLTIP:
		gnop_widget_set_tooltip(state->widget, state->buf->str);
		state->state = INSIDE_WIDGET;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_WIDGET_LABEL:
		gnop_pane_base_set_label(&state->widget->base,
					 _(state->buf->str));
		state->state = INSIDE_WIDGET;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_WIDGET_LEVELS:
		gnop_pane_base_set_levels_string(&state->widget->base,
						 state->buf->str);
		state->state = INSIDE_WIDGET;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_WIDGET_EXPAND:
		if(state->buf->str[0] == 't' || state->buf->str[0] == 'T' ||
		   state->buf->str[0] == 'y' || state->buf->str[0] == 'Y' ||
		   atoi(state->buf->str) != 0)
			gnop_widget_set_expand(state->widget, TRUE);
		else
			gnop_widget_set_expand(state->widget, FALSE);
		state->state = INSIDE_WIDGET;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_WIDGET_TYPE:
		gnop_widget_set_widget_type_string(state->widget,
						   state->buf->str);
		state->state = INSIDE_WIDGET;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_ARGUMENT:
		if( ! state->arg_name ||
		    ! state->arg_value) {
			g_warning(_("Argument requires both name and value"));
			g_free(state->arg_name);
			g_free(state->arg_value);
			state->arg_name = NULL;
			state->arg_value = NULL;
			state->state = INSIDE_WIDGET;
			break;
		}

		if(state->arg_translatable)
			gnop_widget_add_argument(state->widget,
						 state->arg_name,
						 _(state->arg_value));
		else
			gnop_widget_add_argument(state->widget,
						 state->arg_name,
						 state->arg_value);
		g_free(state->arg_name);
		g_free(state->arg_value);
		state->arg_name = NULL;
		state->arg_value = NULL;
		state->state = INSIDE_WIDGET;
		break;

	case INSIDE_ARGUMENT_NAME:
		g_free(state->arg_name);
		state->arg_name = g_strdup(state->buf->str);
		state->state = INSIDE_ARGUMENT;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_ARGUMENT_VALUE:
		g_free(state->arg_value);
		state->arg_value = g_strdup(state->buf->str);
		state->state = INSIDE_ARGUMENT;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_ARGUMENT_TRANSLATABLE:
		if(state->buf->str[0] == 't' || state->buf->str[0] == 'T' ||
		   state->buf->str[0] == 'y' || state->buf->str[0] == 'Y' ||
		   atoi(state->buf->str) != 0)
			state->arg_translatable = TRUE;
		else
			state->arg_translatable = FALSE;
		state->state = INSIDE_ARGUMENT;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_OPTION:
		if( ! state->opt_value) {
			g_warning(_("Option requires a value"));
			g_free(state->opt_label);
			state->opt_label = NULL;
			state->opt_value = NULL;
			state->state = INSIDE_WIDGET;
			break;
		}

		if(state->opt_label)
			gnop_widget_add_option(state->widget,
					       _(state->opt_label),
					       state->opt_value);
		else
			gnop_widget_add_option(state->widget, NULL,
					       state->opt_value);
		state->state = INSIDE_WIDGET;
		break;

	case INSIDE_OPTION_LABEL:
		g_free(state->opt_label);
		state->opt_label = g_strdup(state->buf->str);
		state->state = INSIDE_OPTION;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_OPTION_VALUE:
		g_free(state->opt_value);
		state->opt_value = g_strdup(state->buf->str);
		state->state = INSIDE_OPTION;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	default:
		g_warning("UHH? (state %d)", state->state);
		break;
	}
}


/* stolen from the SAX interface tutorial */
static int
myXmlSAXParseFile(xmlSAXHandlerPtr sax, void *user_data, const char *filename)
{
	int ret = 0;                                        
	xmlParserCtxtPtr ctxt;                              

	ctxt = xmlCreateFileParserCtxt(filename);    
	if (ctxt == NULL) return -1;                 
	ctxt->sax = sax;                                    
	ctxt->userData = user_data;    

	xmlParseDocument(ctxt);      

	if (ctxt->wellFormed)        
		ret = 0;                                   
	else                                               
		ret = -1;          
	if (sax != NULL)                                   
		ctxt->sax = NULL;
	xmlFreeParserCtxt(ctxt);     

	return ret;
}

static int
myXmlSAXParseMemory(xmlSAXHandlerPtr sax, void *user_data,
		    const char *buffer, int size)
{
	int ret = 0;                                        
	xmlParserCtxtPtr ctxt;                              

	/* XXX: casting to (char *) should be safe */
	ctxt = xmlCreateMemoryParserCtxt((char *)buffer, size);    
	if (ctxt == NULL) return -1;                 
	ctxt->sax = sax;                                    
	ctxt->userData = user_data;    

	xmlParseDocument(ctxt);      

	if (ctxt->wellFormed)        
		ret = 0;                                   
	else                                               
		ret = -1;          
	if (sax != NULL)                                   
		ctxt->sax = NULL;
	xmlFreeParserCtxt(ctxt);     

	return ret;
}

static void
gnop_file_add_element(GnopFile *gf, GnopElement *ge)
{
	GnopElement *old;
	old = g_hash_table_lookup(gf->element_hash,
				  ge->conf_path);
	if(old) {
		g_hash_table_remove(gf->element_hash,
				    ge->conf_path);
		gnop_element_unref(old);
	}
	g_hash_table_insert(gf->element_hash,
			    ge->conf_path, ge);
}

static void
unref_elements(gpointer key, gpointer data, gpointer user_data)
{
	GnopElement *ge = data;
	gnop_element_unref(ge);
}

static void
gnop_file_destroy(GnopFile *gf)
{
	GList *li;
	g_hash_table_foreach(gf->element_hash,
			     unref_elements, NULL);
	g_hash_table_destroy(gf->element_hash);
	gf->element_hash = NULL;

	g_free(gf->file_name);
	gf->file_name = NULL;

	g_free(gf->base_conf_path);
	gf->base_conf_path = NULL;

	g_free(gf->dialog_title);
	gf->dialog_title = NULL;

	g_free(gf->help_name);
	gf->help_name = NULL;
	g_free(gf->help_path);
	gf->help_path = NULL;

	g_list_foreach(gf->panes, (GFunc)gnop_pane_base_unref, NULL);
	gf->panes = NULL;

	for(li = gf->levels; li != NULL; li = li->next) {
		GnopLevel *level = li->data;
		li->data = NULL;
		g_free(level->name);
		g_free(level->base_conf_path);
		g_free(level);
	}
	g_list_free(gf->levels);
	gf->levels = NULL;

	g_free(gf);
}

static char *
find_gnop_file(const char *file)
{
	char *s, *ss, *path;
	char **pathv;
	int i;
	GList *li;

	if(g_file_exists(file))
		return g_strdup(file);

	/* an absolute path is just checked */
	if(file[0] == G_DIR_SEPARATOR)
		return NULL;

	for(li = gnop_directories; li != NULL; li = li->next) {
		s = g_concat_dir_and_file(li->data, file);
		if(g_file_exists(s))
			return s;
		g_free(s);
	}

	s = gnome_datadir_file(file);
	if(s)
		return s;

	ss = g_strconcat(g_get_prgname(), "/", file, NULL);
	s = gnome_datadir_file(ss);
	g_free(ss);
	if(s)
		return s;

	path = g_getenv("GNOME_PATH");
	if( ! path)
		return NULL;
	pathv = g_strsplit(path, ":", 0);
	g_free(path);
	for(i = 0; pathv[i] != NULL; i++) {
		s = g_strconcat(pathv[i], "/share/", file, NULL);
		if(g_file_exists(s)) {
			g_strfreev(pathv);
			return s;
		}
		g_free(s);
		s = g_strconcat(pathv[i], "/share/", g_get_prgname(), "/",
				file, NULL);
		if(g_file_exists(s)) {
			g_strfreev(pathv);
			return s;
		}
		g_free(s);
	}
	g_strfreev(pathv);

	return NULL;
}

static void
clear_state(State *state)
{
	if(state->gf)
		gnop_file_unref(state->gf);
	state->gf = NULL;

	if(state->sens)
		gnop_sensitivity_destroy(state->sens);
	state->sens = NULL;

	if(state->element)
		gnop_element_unref(state->element);
	state->element = NULL;

	if(state->pane)
		gnop_pane_base_unref(&state->pane->base);
	state->pane = NULL;

	if(state->group)
		gnop_pane_base_unref(&state->group->base);
	state->group = NULL;

	if(state->widget)
		gnop_pane_base_unref(&state->widget->base);
	state->widget = NULL;

	g_free(state->arg_name);
	state->arg_name = NULL;
	g_free(state->arg_value);
	state->arg_value = NULL;

	g_free(state->opt_label);
	state->opt_label = NULL;
	g_free(state->opt_value);
	state->opt_value = NULL;

	if(state->level)
		gnop_level_destroy(state->level);
	state->level = NULL;
}

GnopFile *
gnop_file_load_xml(const char *file)
{
	State state = {0};
	GnopFile *gf;
	char *found_file;

	g_return_val_if_fail(file != NULL, NULL);

	if( ! parser_inited)
		parser_init();

	gf = g_hash_table_lookup(file_hash, file);
	if(gf) {
		return gnop_file_ref(gf);
	}

	found_file = find_gnop_file(file);

	if( ! found_file) {
		g_warning(_("Cannot find file '%s'"), file);
		return NULL;
	}

	gf = g_hash_table_lookup(file_hash, found_file);
	if(gf) {
		g_free(found_file);
		return gnop_file_ref(gf);
	}

	state.gf = g_new0(GnopFile, 1);
	state.gf->base_conf_path = g_strdup("/");
	state.gf->cached = FALSE;
	state.gf->element_hash = g_hash_table_new(g_str_hash, g_str_equal);
	state.gf->file_name = found_file;

	if (myXmlSAXParseFile(&handler, &state, file) < 0) {
		gnop_file_destroy(state.gf);
		gf = NULL;
	} else {
		g_hash_table_insert(file_hash, g_strdup(file), state.gf);
		/* if cached is on, the object will not get destroyed on the
		 * last unref, but it will stick around in the hash */
		state.gf->cached = TRUE;
		/* ref for the user */
		gf = gnop_file_ref(state.gf);
		state.gf = NULL;
	}

	clear_state(&state);

	return gf;
}

GnopFile *
gnop_file_load_xml_from_memory(const char *buffer, int size)
{
	State state = {0};

	g_return_val_if_fail(buffer != NULL, NULL);
	g_return_val_if_fail(size > 0, NULL);

	if( ! parser_inited)
		parser_init();

	state.gf = g_new0(GnopFile, 1);
	state.gf->cached = FALSE;
	state.gf->element_hash = g_hash_table_new(g_str_hash, g_str_equal);

	if (myXmlSAXParseMemory(&handler, &state, buffer, size) < 0) {
		clear_state(&state);
		return NULL;
	} else {
		GnopFile *gf = state.gf;
		state.gf = NULL;
		clear_state(&state);
		return gf;
	}
}

GnopFile *
gnop_file_ref(GnopFile *gf)
{
	g_return_val_if_fail(gf != NULL, NULL);

	gf->refcount ++;

	return gf;
}

void
gnop_file_unref(GnopFile *gf)
{
	g_return_if_fail(gf != NULL);

	gf->refcount --;

	if(gf->refcount == 0 && !gf->cached)
		gnop_file_destroy(gf);
}

static gboolean
kill_unused_gnopfiles(gpointer key, gpointer data, gpointer user_data)
{
	GnopFile *gf = data;
	if(gf->cached && gf->refcount <= 0) {
		gf->cached = FALSE;
		gnop_file_destroy(gf);

		/* free the filename */
		g_free(data);
		return TRUE;
	}
	return FALSE;
}

void
gnop_parser_free_unused_data(void)
{
	if(parser_inited) {
		g_hash_table_foreach_remove(file_hash, kill_unused_gnopfiles, NULL);

		if(g_hash_table_size(file_hash) == 0) {
			g_hash_table_destroy(file_hash);
			file_hash = NULL;
			parser_inited = FALSE;
		}
	}

	if(comparison_hash != NULL) {
		g_hash_table_destroy(comparison_hash);
		comparison_hash = NULL;
	}
}

static void
add_elements_to_list(gpointer key, gpointer data, gpointer user_data)
{
	GList **list = user_data;
	*list = g_list_prepend(*list, key);
}

/* list of internal strings, DON'T FREE THEM, just free the list */
GList *
gnop_file_get_conf_paths(GnopFile *gf)
{
	GList *list = NULL;
	g_return_val_if_fail(gf != NULL, NULL);

	g_hash_table_foreach(gf->element_hash, add_elements_to_list, &list);

	return list;
}

GnopElement *
gnop_file_lookup_element(GnopFile *gf, const char *conf_path)
{
	g_return_val_if_fail(gf != NULL, NULL);
	g_return_val_if_fail(conf_path != NULL, NULL);

	return g_hash_table_lookup(gf->element_hash, conf_path);
}

static void
foreach_elements(gpointer key, gpointer data, gpointer user_data)
{
	gpointer *fedata = user_data;
	GnopElementFunc func = fedata[0];

	func((GnopElement *)data, fedata[1]);
}

void
gnop_file_foreach_element(GnopFile *gf, GnopElementFunc func, gpointer data)
{
	gpointer fedata[2];

	g_return_if_fail(gf != NULL);
	g_return_if_fail(func != NULL);

	fedata[0] = func;
	fedata[1] = data;

	g_hash_table_foreach(gf->element_hash, foreach_elements, fedata);
}

GnopLevel *
gnop_level_new(void)
{
	GnopLevel *level = g_new0(GnopLevel, 1);
	return level;
}

void
gnop_level_destroy(GnopLevel *level)
{
	g_return_if_fail(level != NULL);

	g_free(level->name);
	level->name = NULL;
	g_free(level->base_conf_path);
	level->base_conf_path = NULL;
	g_free(level);
}


GList *
gnop_file_get_levels(GnopFile *gf)
{
	GList *li, *list = NULL;

	g_return_val_if_fail(gf != NULL, NULL);

	for(li = gf->levels; li != NULL; li = li->next) {
		GnopLevel *level = li->data;
		list = g_list_prepend(list, level->name);
	}

	return list;
}

const char *
gnop_file_get_level_base_conf_path(GnopFile *gf, const char *level_name)
{
	GList *li;

	g_return_val_if_fail(gf != NULL, NULL);
	g_return_val_if_fail(level_name != NULL, NULL);

	for(li = gf->levels; li != NULL; li = li->next) {
		GnopLevel *level = li->data;
		if(strcmp(level_name, level->name) == 0)
			return level->base_conf_path;
	}
	return NULL;
}
