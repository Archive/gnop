/* GnoP: Utility routines
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <stdlib.h>

#include "gnoputil.h"

gboolean
gnop_bool_from_string(const char *s)
{
	if(!s)
		return FALSE;
	if(s[0] == 'T' || s[0] == 't' ||
	   s[0] == 'Y' || s[0] == 'y' ||
	   atoi(s) != 0)
		return TRUE;
	else
		return FALSE;
}

gboolean
gnop_gconf_value_equal(GConfValue *v1, GConfValue *v2)
{
	if(v1->type != v2->type)
		return FALSE;
	switch(v1->type) {
	case GCONF_VALUE_STRING:
		if(strcmp(gconf_value_string(v1), gconf_value_string(v2)) == 0)
			return TRUE;
		else
			return FALSE;
	case GCONF_VALUE_INT:
		if(gconf_value_int(v1) == gconf_value_int(v2))
			return TRUE;
		else
			return FALSE;
	case GCONF_VALUE_FLOAT:
		/* Eeek, float comparison, might be ok in this case though I guess */
		if(gconf_value_float(v1) == gconf_value_float(v2))
			return TRUE;
		else
			return FALSE;
	case GCONF_VALUE_BOOL:
		if((gconf_value_bool(v1) ? 1 : 0) ==
		   (gconf_value_bool(v2) ? 1 : 0))
			return TRUE;
		else
			return FALSE;
	case GCONF_VALUE_LIST:
		{
			GConfValueType type = gconf_value_list_type(v1);
			GSList *li1, *li2;
			if(type != gconf_value_list_type(v2))
				return FALSE;

			for(li1 = gconf_value_list(v1), li2 = gconf_value_list(v2);
			    li1 != NULL && li2 != NULL;
			    li1 = li1->next, li2 = li2->next) {
				if( ! gnop_gconf_value_equal(li1->data, li2->data))
					return FALSE;
			}
			/* At least one of them is NULL, so we're effectively
			 * testing for NULL of both */
			if(li1 != li2)
				return FALSE;
			return TRUE;
		}
	case GCONF_VALUE_PAIR:
		if( ! gnop_gconf_value_equal(gconf_value_car(v1),
					     gconf_value_car(v2)))
			return FALSE;
		if( ! gnop_gconf_value_equal(gconf_value_cdr(v1),
					     gconf_value_cdr(v2)))
			return FALSE;
		return TRUE;
	default:
		g_warning(_("Unknown GConfValue type to compare"));
		break;
	}
	return FALSE;
}

GConfValueType
gnop_gconf_value_type_from_gnop_type(GnopType type)
{
	switch(type) {

	case GNOP_TYPE_STRING:
		return GCONF_VALUE_STRING;

	case GNOP_TYPE_INT:
		return GCONF_VALUE_INT;

	case GNOP_TYPE_FLOAT:
		return GCONF_VALUE_FLOAT;

	case GNOP_TYPE_BOOL:
		return GCONF_VALUE_BOOL;

	case GNOP_TYPE_LIST_OF_STRINGS:
	case GNOP_TYPE_LIST_OF_INTS:
	case GNOP_TYPE_LIST_OF_FLOATS:
	case GNOP_TYPE_LIST_OF_BOOLS:
		return GCONF_VALUE_LIST;

	case GNOP_TYPE_PAIR_STRING_STRING:
	case GNOP_TYPE_PAIR_STRING_INT:
	case GNOP_TYPE_PAIR_STRING_FLOAT:
	case GNOP_TYPE_PAIR_STRING_BOOL:
	case GNOP_TYPE_PAIR_INT_STRING:
	case GNOP_TYPE_PAIR_INT_INT:
	case GNOP_TYPE_PAIR_INT_FLOAT:
	case GNOP_TYPE_PAIR_INT_BOOL:
	case GNOP_TYPE_PAIR_FLOAT_STRING:
	case GNOP_TYPE_PAIR_FLOAT_INT:
	case GNOP_TYPE_PAIR_FLOAT_FLOAT:
	case GNOP_TYPE_PAIR_FLOAT_BOOL:
	case GNOP_TYPE_PAIR_BOOL_STRING:
	case GNOP_TYPE_PAIR_BOOL_INT:
	case GNOP_TYPE_PAIR_BOOL_FLOAT:
	case GNOP_TYPE_PAIR_BOOL_BOOL:
		return GCONF_VALUE_PAIR;

	default:
		return GCONF_VALUE_INVALID;
	}
}

GnopType
gnop_type_from_gconf_value(GConfValue *value)
{
	g_return_val_if_fail(value != NULL, GNOP_TYPE_INVALID);

	switch(value->type) {

	case GCONF_VALUE_STRING:
		return GNOP_TYPE_STRING;
	case GCONF_VALUE_INT:
		return GNOP_TYPE_INT;
	case GCONF_VALUE_FLOAT:
		return GNOP_TYPE_FLOAT;
	case GCONF_VALUE_BOOL:
		return GNOP_TYPE_BOOL;

	case GCONF_VALUE_LIST:
		{
			GConfValueType type = gconf_value_list_type(value);
			switch(type) {
			case GCONF_VALUE_STRING:
				return GNOP_TYPE_LIST_OF_STRINGS;
			case GCONF_VALUE_INT:
				return GNOP_TYPE_LIST_OF_INTS;
			case GCONF_VALUE_FLOAT:
				return GNOP_TYPE_LIST_OF_FLOATS;
			case GCONF_VALUE_BOOL:
				return GNOP_TYPE_LIST_OF_BOOLS;
			default:
				return GNOP_TYPE_INVALID;
			}
		}

	case GCONF_VALUE_PAIR:
		{
			GConfValue *car = gconf_value_car(value);
			GConfValue *cdr = gconf_value_cdr(value);
			switch(car->type) {
			case GCONF_VALUE_STRING:
				switch(cdr->type) {
				case GCONF_VALUE_STRING:
					return GNOP_TYPE_PAIR_STRING_STRING;
				case GCONF_VALUE_INT:
					return GNOP_TYPE_PAIR_STRING_INT;
				case GCONF_VALUE_FLOAT:
					return GNOP_TYPE_PAIR_STRING_FLOAT;
				case GCONF_VALUE_BOOL:
					return GNOP_TYPE_PAIR_STRING_BOOL;
				default:
					return GNOP_TYPE_INVALID;
				}
			case GCONF_VALUE_INT:
				switch(cdr->type) {
				case GCONF_VALUE_STRING:
					return GNOP_TYPE_PAIR_INT_STRING;
				case GCONF_VALUE_INT:
					return GNOP_TYPE_PAIR_INT_INT;
				case GCONF_VALUE_FLOAT:
					return GNOP_TYPE_PAIR_INT_FLOAT;
				case GCONF_VALUE_BOOL:
					return GNOP_TYPE_PAIR_INT_BOOL;
				default:
					return GNOP_TYPE_INVALID;
				}
			case GCONF_VALUE_FLOAT:
				switch(cdr->type) {
				case GCONF_VALUE_STRING:
					return GNOP_TYPE_PAIR_FLOAT_STRING;
				case GCONF_VALUE_INT:
					return GNOP_TYPE_PAIR_FLOAT_INT;
				case GCONF_VALUE_FLOAT:
					return GNOP_TYPE_PAIR_FLOAT_FLOAT;
				case GCONF_VALUE_BOOL:
					return GNOP_TYPE_PAIR_FLOAT_BOOL;
				default:
					return GNOP_TYPE_INVALID;
				}
			case GCONF_VALUE_BOOL:
				switch(cdr->type) {
				case GCONF_VALUE_STRING:
					return GNOP_TYPE_PAIR_BOOL_STRING;
				case GCONF_VALUE_INT:
					return GNOP_TYPE_PAIR_BOOL_INT;
				case GCONF_VALUE_FLOAT:
					return GNOP_TYPE_PAIR_BOOL_FLOAT;
				case GCONF_VALUE_BOOL:
					return GNOP_TYPE_PAIR_BOOL_BOOL;
				default:
					return GNOP_TYPE_INVALID;
				}
			default:
				return GNOP_TYPE_INVALID;
			}
		}

	default:
		return GNOP_TYPE_INVALID;
	}

}

static GSList *
parse_list(const char *s)
{
	char *buf;
	const char *p;
	GSList *list;
	gboolean escaped = FALSE;
	int i;

	buf = g_malloc(strlen(s) + 1);

	i = 0;
	list = NULL;
	for(p = s; *p != '\0'; p++) {
		if(escaped) {
			escaped = FALSE;
			buf[i++] = *p;
		} else if(*p == '\\') {
			escaped = TRUE;
		} else if(*p == ' ') {
			buf[i] = '\0';
			list = g_slist_prepend(list, g_strdup(buf));
			i = 0;
		} else {
			buf[i++] = *p;
		}
	}
	buf[i] = '\0';
	list = g_slist_prepend(list, g_strdup(buf));

	g_free(buf);

	return g_slist_reverse(list);
}

static void
set_list(GConfValue *value, const char *s, GnopType type)
{
	GSList *list, *li;
	GConfValueType gconf_type = gnop_gconf_value_type_from_gnop_type(type);

	gconf_value_set_list_type(value, gconf_type);

	list = parse_list(s);
	for(li = list; li != NULL; li = li->next) {
		char *v = li->data;
		li->data = gnop_gconf_value_from_string(v, type);

		/* Just for sanity */
		if( ! li->data)
			li->data = gconf_value_new(gconf_type);

		g_free(v);
	}

	gconf_value_set_list_nocopy(value, list);
}

static void
set_pair(GConfValue *value, const char *s, GnopType type_car, GnopType type_cdr)
{
	GSList *list;
	GConfValueType gconf_type_car = gnop_gconf_value_type_from_gnop_type(type_car);
	GConfValueType gconf_type_cdr = gnop_gconf_value_type_from_gnop_type(type_cdr);

	list = parse_list(s);

	if(list && list->data) {
		GConfValue *v = gnop_gconf_value_from_string(list->data, type_car);

		/* Just for sanity */
		if( ! v)
			v = gconf_value_new(gconf_type_car);

		gconf_value_set_car_nocopy(value, v);
	} else {
		GConfValue *v = gconf_value_new(gconf_type_car);
		gconf_value_set_car_nocopy(value, v);
	}

	if(list && list->next && list->next->data) {
		GConfValue *v = gnop_gconf_value_from_string(list->next->data, type_cdr);

		/* Just for sanity */
		if( ! v)
			v = gconf_value_new(gconf_type_cdr);

		gconf_value_set_cdr_nocopy(value, v);
	} else {
		GConfValue *v = gconf_value_new(gconf_type_cdr);
		gconf_value_set_cdr_nocopy(value, v);
	}

	g_slist_foreach(list, (GFunc)g_free, NULL);
	g_slist_free(list);
}

GConfValue *
gnop_gconf_value_from_string(const char *s, GnopType type)
{
	GConfValue *value;
	GConfValueType gconf_type;

	g_return_val_if_fail(s != NULL, NULL);

	gconf_type = gnop_gconf_value_type_from_gnop_type(type);

	if(gconf_type == GCONF_VALUE_INVALID) {
		g_warning(_("Invalid type"));
		return NULL;
	}

	value = gconf_value_new(gconf_type);

	switch(type) {
	case GNOP_TYPE_STRING:
		gconf_value_set_string(value, s);
		break;
	case GNOP_TYPE_INT:
		gconf_value_set_int(value, atoi(s));
		break;
	case GNOP_TYPE_FLOAT:
		gconf_value_set_float(value, atof(s));
		break;
	case GNOP_TYPE_BOOL:
		if(s[0] == 't' || s[0] == 'T' ||
		   s[0] == 'y' || s[0] == 'Y' ||
		   atoi(s) != 0)
			gconf_value_set_bool(value, TRUE);
		else
			gconf_value_set_bool(value, FALSE);
		break;

	case GNOP_TYPE_LIST_OF_STRINGS:
		set_list(value, s, GNOP_TYPE_STRING);
		break;
	case GNOP_TYPE_LIST_OF_INTS:
		set_list(value, s, GNOP_TYPE_INT);
		break;
	case GNOP_TYPE_LIST_OF_FLOATS:
		set_list(value, s, GNOP_TYPE_FLOAT);
		break;
	case GNOP_TYPE_LIST_OF_BOOLS:
		set_list(value, s, GNOP_TYPE_BOOL);
		break;

	case GNOP_TYPE_PAIR_STRING_STRING:
		set_pair(value, s, GNOP_TYPE_STRING, GNOP_TYPE_STRING);
		break;
	case GNOP_TYPE_PAIR_STRING_INT:
		set_pair(value, s, GNOP_TYPE_STRING, GNOP_TYPE_INT);
		break;
	case GNOP_TYPE_PAIR_STRING_FLOAT:
		set_pair(value, s, GNOP_TYPE_STRING, GNOP_TYPE_FLOAT);
		break;
	case GNOP_TYPE_PAIR_STRING_BOOL:
		set_pair(value, s, GNOP_TYPE_STRING, GNOP_TYPE_BOOL);
		break;
	case GNOP_TYPE_PAIR_INT_STRING:
		set_pair(value, s, GNOP_TYPE_INT, GNOP_TYPE_STRING);
		break;
	case GNOP_TYPE_PAIR_INT_INT:
		set_pair(value, s, GNOP_TYPE_INT, GNOP_TYPE_INT);
		break;
	case GNOP_TYPE_PAIR_INT_FLOAT:
		set_pair(value, s, GNOP_TYPE_INT, GNOP_TYPE_FLOAT);
		break;
	case GNOP_TYPE_PAIR_INT_BOOL:
		set_pair(value, s, GNOP_TYPE_INT, GNOP_TYPE_BOOL);
		break;
	case GNOP_TYPE_PAIR_FLOAT_STRING:
		set_pair(value, s, GNOP_TYPE_FLOAT, GNOP_TYPE_STRING);
		break;
	case GNOP_TYPE_PAIR_FLOAT_INT:
		set_pair(value, s, GNOP_TYPE_FLOAT, GNOP_TYPE_INT);
		break;
	case GNOP_TYPE_PAIR_FLOAT_FLOAT:
		set_pair(value, s, GNOP_TYPE_FLOAT, GNOP_TYPE_FLOAT);
		break;
	case GNOP_TYPE_PAIR_FLOAT_BOOL:
		set_pair(value, s, GNOP_TYPE_FLOAT, GNOP_TYPE_BOOL);
		break;
	case GNOP_TYPE_PAIR_BOOL_STRING:
		set_pair(value, s, GNOP_TYPE_BOOL, GNOP_TYPE_STRING);
		break;
	case GNOP_TYPE_PAIR_BOOL_INT:
		set_pair(value, s, GNOP_TYPE_BOOL, GNOP_TYPE_INT);
		break;
	case GNOP_TYPE_PAIR_BOOL_FLOAT:
		set_pair(value, s, GNOP_TYPE_BOOL, GNOP_TYPE_FLOAT);
		break;
	case GNOP_TYPE_PAIR_BOOL_BOOL:
		set_pair(value, s, GNOP_TYPE_BOOL, GNOP_TYPE_BOOL);
		break;

	default:
		/* This should never really happen */
		gconf_value_destroy(value);
		return NULL;
	}

	return value;
}

static char *
escape_string(const char *s, gboolean *new_string)
{
	const char *p;
	char *ret;
	int i, to_escape = 0;
	int len = 0;

	*new_string = FALSE;

	for(p = s; *p != '\0'; p++) {
		if(*p == ' ')
			to_escape ++;
		len ++;
	}

	if(to_escape == 0) {
		*new_string = FALSE;
		return (char *)s;
	}

	ret = g_malloc(len + to_escape + 1);

	i = 0;
	for(p = s; *p != '\0'; p++) {
		if(*p == ' ')
			ret[i++] = '\\';
		ret[i++] = *p;
	}
	ret[i++] = '\0';

	*new_string = TRUE;
	return ret;
}

char *
gnop_string_from_gconf_value(GConfValue *value)
{
	g_return_val_if_fail(value != NULL, NULL);

	switch(value->type) {
	case GCONF_VALUE_STRING:
		return g_strdup(gconf_value_string(value));
	case GCONF_VALUE_INT:
		return g_strdup_printf("%d", gconf_value_int(value));
	case GCONF_VALUE_FLOAT:
		return g_strdup_printf("%g", gconf_value_float(value));
	case GCONF_VALUE_BOOL:
		return g_strdup(gconf_value_bool(value) ? "True" : "False");

	case GCONF_VALUE_LIST:
		{
			char *ret, *sep;
			GSList *li;
			GString *gs;
			gs = g_string_new(NULL);
			sep = "";

			for(li = gconf_value_list(value); li != NULL; li = li->next) {
				char *s = gnop_string_from_gconf_value(li->data);
				gboolean free_ns = FALSE;
				char *ns = escape_string(s, &free_ns);

				g_string_append(gs, sep);
				g_string_append(gs, ns);

				sep = " ";

				if(free_ns) g_free(ns);
				g_free(s);
			}

			ret = gs->str;
			g_string_free(gs, FALSE);
			return ret;
		}
	case GCONF_VALUE_PAIR:
		{
			char *s1, *s2;
			char *ns1, *ns2;
			char *ret;
			gboolean free1 = FALSE, free2 = FALSE;

			s1 = gnop_string_from_gconf_value(gconf_value_car(value));
			s2 = gnop_string_from_gconf_value(gconf_value_cdr(value));

			ns1 = escape_string(s1, &free1);
			ns2 = escape_string(s2, &free2);

			ret = g_strconcat(ns1, " ", ns2, NULL);

			if(free1) g_free(ns1);
			if(free2) g_free(ns2);

			g_free(s1);
			g_free(s2);

			return ret;
		}
	default:
		g_warning(_("Unsupported gconf value type to convert to string"));
		return NULL;
	}

}
