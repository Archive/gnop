/* GnoP: The types enum
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef GNOP_TYPE_H
#define GNOP_TYPE_H

/* These correspond to GConf Types */
typedef enum {
	GNOP_TYPE_INVALID,

	/* Basic types */
	GNOP_TYPE_STRING,
	GNOP_TYPE_INT,
	GNOP_TYPE_FLOAT,
	GNOP_TYPE_BOOL,

	/* List types */
	GNOP_TYPE_LIST_OF_STRINGS,
	GNOP_TYPE_LIST_OF_INTS,
	GNOP_TYPE_LIST_OF_FLOATS,
	GNOP_TYPE_LIST_OF_BOOLS,

	/* Pair types */
	GNOP_TYPE_PAIR_STRING_STRING,
	GNOP_TYPE_PAIR_STRING_INT,
	GNOP_TYPE_PAIR_STRING_FLOAT,
	GNOP_TYPE_PAIR_STRING_BOOL,

	GNOP_TYPE_PAIR_INT_STRING,
	GNOP_TYPE_PAIR_INT_INT,
	GNOP_TYPE_PAIR_INT_FLOAT,
	GNOP_TYPE_PAIR_INT_BOOL,

	GNOP_TYPE_PAIR_FLOAT_STRING,
	GNOP_TYPE_PAIR_FLOAT_INT,
	GNOP_TYPE_PAIR_FLOAT_FLOAT,
	GNOP_TYPE_PAIR_FLOAT_BOOL,

	GNOP_TYPE_PAIR_BOOL_STRING,
	GNOP_TYPE_PAIR_BOOL_INT,
	GNOP_TYPE_PAIR_BOOL_FLOAT,
	GNOP_TYPE_PAIR_BOOL_BOOL,

	GNOP_TYPE_LAST
} GnopType;

#endif /* GNOP_TYPE_H */
