/* GnoP: Utility routines
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef GNOPUTIL_H
#define GNOPUTIL_H

#include <glib.h>
#include <gconf/gconf-value.h>
#include "gnop-type.h"

#ifdef __cplusplus
extern "C" {
#endif

gboolean	gnop_bool_from_string			(const char *s);
gboolean	gnop_gconf_value_equal			(GConfValue *v1,
							 GConfValue *v2);

GConfValueType	gnop_gconf_value_type_from_gnop_type	(GnopType type);
GnopType	gnop_type_from_gconf_value		(GConfValue *value);

/* Unlike the gconf equivalents, these
 * will try to do "something".  They use the gnome_config style vector stuff
 * using spaces. */
GConfValue *	gnop_gconf_value_from_string		(const char *s, GnopType type);
char *		gnop_string_from_gconf_value		(GConfValue *value);

#ifdef __cplusplus
}
#endif

#endif /* GNOPUTIL_H */
