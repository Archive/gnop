/* GnoP: the parser using SAX interface
 * Author: George Lebl
 * (c) 1999 Free Software Foundation
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef GNOPPARSER_H
#define GNOPPARSER_H

#include "gnopelement.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _GnopFile GnopFile;
struct _GnopFile {
	int refcount;
	gboolean cached;
	char *file_name;
	char *base_conf_path;
	char *dialog_title;
	char *help_name;
	char *help_path;
	GHashTable *element_hash;
	GList *panes;
	GList *levels;
};

typedef struct _GnopLevel GnopLevel;
struct _GnopLevel {
	char *name;
	char *base_conf_path;
};

typedef void (*GnopElementFunc)(GnopElement *element, gpointer data);

GnopFile *	gnop_file_load_xml		(const char *file);
GnopFile *	gnop_file_load_xml_from_memory	(const char *buffer, int size);
GnopFile *	gnop_file_ref			(GnopFile *gf);
void		gnop_file_unref			(GnopFile *gf);

/* list of internal strings, DON'T FREE THEM, just free the list */
GList *		gnop_file_get_conf_paths	(GnopFile *gf);

GnopElement *	gnop_file_lookup_element	(GnopFile *gf,
						 const char *conf_path);

void		gnop_file_foreach_element	(GnopFile *gf,
						 GnopElementFunc func,
						 gpointer data);

/* list of internal strings, DON'T FREE THEM, just free the list */
GList *		gnop_file_get_levels		(GnopFile *gf);
const char *	gnop_file_get_level_base_conf_path(GnopFile *gf,
						   const char *level_name);

/* kill all unused GnopFile structures */
void		gnop_parser_free_unused_data	(void);

/* Really for internal use only */
GnopLevel *	gnop_level_new			(void);
void		gnop_level_destroy		(GnopLevel *level);

#ifdef __cplusplus
}
#endif


#endif /* GNOPPARSER_H */
