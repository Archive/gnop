/* GnoP: widget utility routines
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef GNOPWIDGETUTIL_H
#define GNOPWIDGETUTIL_H

#ifdef __cplusplus
extern "C" {
#endif

void		gnop_gtk_entry_set_double	(GtkEntry *entry,
						 double number);
void		gnop_gnome_number_entry_set_double(GnomeNumberEntry *gentry,
						   double number);
void		gnop_gnome_entry_set_double	(GnomeEntry *gentry,
						 double number);

void		gnop_gtk_entry_set_long		(GtkEntry *entry,
						 long number);

void		gnop_gnome_number_entry_set_long(GnomeNumberEntry *gentry,
						 long number);

void		gnop_gnome_entry_set_long	(GnomeEntry *gentry,
						 long number);
void		gnop_gnome_entry_set_text	(GnomeEntry *gentry,
						 const char *text);

void		gnop_gnome_file_entry_set_text	(GnomeFileEntry *gentry,
						 const char *text);

void		gnop_gnome_pixmap_entry_set_text(GnomePixmapEntry *gentry,
						 const char *text);

double		gnop_gtk_entry_get_double	(GtkEntry *entry);

long		gnop_gtk_entry_get_long		(GtkEntry *entry);

double		gnop_gnome_entry_get_double	(GnomeEntry *gentry);

long		gnop_gnome_entry_get_long	(GnomeEntry *gentry);

char *		gnop_gnome_entry_get_text	(GnomeEntry *gentry);

char *		gnop_gnome_file_entry_get_text	(GnomeFileEntry *gentry);

char *		gnop_gnome_pixmap_entry_get_text(GnomePixmapEntry *gentry);

char *		gnop_gnome_icon_entry_get_text	(GnomeIconEntry *gentry);

gboolean	gnop_set_object_argument	(GtkObject *object,
						 const char *arg_name,
						 const char *value);

#ifdef __cplusplus
}
#endif

#endif /* GNOPWIDGETUTIL_H */
