/* GnoP: Standard widget interface
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include "gnop-widget-interface.h"

GnopWidgetInterface *
gnop_widget_interface_add(GtkObjectClass *klass)
{
	GnopWidgetInterface *iface;

	g_return_val_if_fail(klass != NULL, NULL);
	g_return_val_if_fail(GTK_IS_OBJECT_CLASS(klass), NULL);

	iface = g_dataset_get_data(klass, "gnop_widget_interface");
	if( ! iface) {
		iface = g_new0(GnopWidgetInterface, 1);

		g_dataset_set_data(klass, "gnop_widget_interface", iface);
	}

	return iface;
}

/* check for the existance of the interface on a widget */
static GnopWidgetInterface *
gnop_widget_interface_get(GtkWidget *w)
{
	g_return_val_if_fail(w != NULL, FALSE);
	g_return_val_if_fail(GTK_IS_WIDGET(w), FALSE);

	return g_dataset_get_data(GTK_OBJECT(w)->klass,
				  "gnop_widget_interface");
}

/* check for the existance of the interface on a widget */
gboolean
gnop_widget_interface_exists(GtkWidget *w)
{
	GnopWidgetInterface *iface;

	g_return_val_if_fail(w != NULL, FALSE);
	g_return_val_if_fail(GTK_IS_WIDGET(w), FALSE);

	iface = gnop_widget_interface_get(w);

	if(iface)
		return TRUE;
	else
		return FALSE;
}

const char *
gnop_widget_interface_changed_signal(GtkWidget *w)
{
	GnopWidgetInterface *iface;

	g_return_val_if_fail(w != NULL, NULL);
	g_return_val_if_fail(GTK_IS_WIDGET(w), NULL);

	iface = gnop_widget_interface_get(w);
	g_return_val_if_fail(iface != NULL, NULL);

	if( ! iface->changed_signal) {
		g_warning(_("Widget specified no changed signal, "
			    "trying \"changed\""));
		return "changed";
	}

	return iface->changed_signal;
}

gboolean
gnop_widget_interface_get_value	(GtkWidget *w,
				 GnopType type,
				 GConfValue **value)
{
	GnopWidgetInterface *iface;

	g_return_val_if_fail(w != NULL, FALSE);
	g_return_val_if_fail(value != NULL, FALSE);
	g_return_val_if_fail(GTK_IS_WIDGET(w), FALSE);

	iface = gnop_widget_interface_get(w);
	g_return_val_if_fail(iface != NULL, FALSE);

	*value = NULL;

	if( ! iface->get_value) {
		g_warning(_("get_value not implemented in widget"));
		return FALSE;
	} else
		return iface->get_value(w, type, value);
}


gboolean
gnop_widget_interface_set_value	(GtkWidget *w,
				 GConfValue *value)
{
	GnopWidgetInterface *iface;

	g_return_val_if_fail(w != NULL, FALSE);
	g_return_val_if_fail(value != NULL, FALSE);
	g_return_val_if_fail(GTK_IS_WIDGET(w), FALSE);

	iface = gnop_widget_interface_get(w);
	g_return_val_if_fail(iface != NULL, FALSE);

	if( ! iface->set_value) 
		g_warning(_("set_value not implemented in widget"));
	else
		return iface->set_value(w, value);
	return FALSE;
}

gboolean
gnop_widget_interface_implements_add_options(GtkWidget *w)
{
	GnopWidgetInterface *iface;

	g_return_val_if_fail(w != NULL, FALSE);
	g_return_val_if_fail(GTK_IS_WIDGET(w), FALSE);

	iface = gnop_widget_interface_get(w);
	g_return_val_if_fail(iface != NULL, FALSE);

	if(iface->add_options)
		return TRUE;
	else
		return FALSE;
}

void
gnop_widget_interface_add_options(GtkWidget *w, GList *gnop_options)
{
	GnopWidgetInterface *iface;

	g_return_if_fail(w != NULL);
	g_return_if_fail(GTK_IS_WIDGET(w));

	iface = gnop_widget_interface_get(w);
	g_return_if_fail(iface != NULL);

	if( ! iface->add_options)
		g_warning(_("add_options not implemented in widget"));
	else
		iface->add_options(w, gnop_options);
}

gboolean
gnop_widget_interface_implements_set_label(GtkWidget *w)
{
	GnopWidgetInterface *iface;

	g_return_val_if_fail(w != NULL, FALSE);
	g_return_val_if_fail(GTK_IS_WIDGET(w), FALSE);

	iface = gnop_widget_interface_get(w);
	g_return_val_if_fail(iface != NULL, FALSE);

	if(iface->set_label)
		return TRUE;
	else
		return FALSE;
}

void
gnop_widget_interface_set_label	(GtkWidget *w, const char *label)
{
	GnopWidgetInterface *iface;

	g_return_if_fail(w != NULL);
	g_return_if_fail(GTK_IS_WIDGET(w));

	iface = gnop_widget_interface_get(w);
	g_return_if_fail(iface != NULL);

	if( ! iface->set_label)
		g_warning(_("set_label not implemented in widget"));
	else
		iface->set_label(w, label);
}
