/* GnoP: widget utility routines
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include "gnopwidgetutil.h"

void
gnop_gtk_entry_set_double(GtkEntry *entry, double number)
{
	char *s = g_strdup_printf("%g", number);
	gtk_entry_set_text(entry, s);
	g_free(s);
}

void
gnop_gnome_number_entry_set_double(GnomeNumberEntry *gentry, double number)
{
	GtkWidget *entry = gnome_number_entry_gtk_entry(gentry);
	gnop_gtk_entry_set_double(GTK_ENTRY(entry), number);
}

void
gnop_gnome_entry_set_double(GnomeEntry *gentry, double number)
{
	GtkWidget *entry = gnome_entry_gtk_entry(gentry);
	gnop_gtk_entry_set_double(GTK_ENTRY(entry), number);
}

void
gnop_gtk_entry_set_long(GtkEntry *entry, long number)
{
	char *s = g_strdup_printf("%ld", number);
	gtk_entry_set_text(entry, s);
	g_free(s);
}

void
gnop_gnome_number_entry_set_long(GnomeNumberEntry *gentry, long number)
{
	GtkWidget *entry = gnome_number_entry_gtk_entry(gentry);
	gnop_gtk_entry_set_long(GTK_ENTRY(entry), number);
}

void
gnop_gnome_entry_set_long(GnomeEntry *gentry, long number)
{
	GtkWidget *entry = gnome_entry_gtk_entry(gentry);
	gnop_gtk_entry_set_long(GTK_ENTRY(entry), number);
}

void
gnop_gnome_entry_set_text(GnomeEntry *gentry, const char *text)
{
	GtkWidget *entry = gnome_entry_gtk_entry(gentry);
	gtk_entry_set_text(GTK_ENTRY(entry), text);
}

void
gnop_gnome_file_entry_set_text(GnomeFileEntry *gentry, const char *text)
{
	GtkWidget *entry = gnome_file_entry_gtk_entry(gentry);
	gtk_entry_set_text(GTK_ENTRY(entry), text);
}

void
gnop_gnome_pixmap_entry_set_text(GnomePixmapEntry *gentry, const char *text)
{
	GtkWidget *entry = gnome_pixmap_entry_gtk_entry(gentry);
	gtk_entry_set_text(GTK_ENTRY(entry), text);
}

double
gnop_gtk_entry_get_double(GtkEntry *entry)
{
	return atof(gtk_entry_get_text(entry));
}

long
gnop_gtk_entry_get_long(GtkEntry *entry)
{
	return atol(gtk_entry_get_text(entry));
}

double
gnop_gnome_entry_get_double(GnomeEntry *gentry)
{
	GtkWidget *entry = gnome_entry_gtk_entry(gentry);
	return gnop_gtk_entry_get_double(GTK_ENTRY(entry));
}

long
gnop_gnome_entry_get_long(GnomeEntry *gentry)
{
	GtkWidget *entry = gnome_entry_gtk_entry(gentry);
	return gnop_gtk_entry_get_long(GTK_ENTRY(entry));
}

char *
gnop_gnome_entry_get_text(GnomeEntry *gentry)
{
	GtkWidget *entry = gnome_entry_gtk_entry(gentry);
	return gtk_entry_get_text(GTK_ENTRY(entry));
}

char *
gnop_gnome_file_entry_get_text(GnomeFileEntry *gentry)
{
	GtkWidget *entry = gnome_file_entry_gtk_entry(gentry);
	return gtk_entry_get_text(GTK_ENTRY(entry));
}

char *
gnop_gnome_pixmap_entry_get_text(GnomePixmapEntry *gentry)
{
	GtkWidget *entry = gnome_pixmap_entry_gtk_entry(gentry);
	return gtk_entry_get_text(GTK_ENTRY(entry));
}

char *
gnop_gnome_icon_entry_get_text(GnomeIconEntry *gentry)
{
	GtkWidget *entry = gnome_icon_entry_gtk_entry(gentry);
	return gtk_entry_get_text(GTK_ENTRY(entry));
}

gboolean
gnop_set_object_argument(GtkObject *object,
			 const char *arg_name,
			 const char *value)
{
	GtkArgInfo *info;
	char *error;
	GtkArg arg;

	g_return_val_if_fail(object != NULL, FALSE);
	g_return_val_if_fail(GTK_IS_OBJECT(object), FALSE);
	g_return_val_if_fail(arg_name != NULL, FALSE);
	g_return_val_if_fail(value != NULL, FALSE);

	error = gtk_object_arg_get_info (GTK_OBJECT_TYPE (object),
					 arg_name, &info);
	if(error) {
		g_warning(_("Cannot set object argument: %s"), error);
		g_free(error);
		return FALSE;
	}

	arg.type = info->type;
	/* It is ok to cast here */
	arg.name = (char *)arg_name;

	switch(info->type) {
	case GTK_TYPE_CHAR:
		if(!value[0]) {
			g_warning(_("Invalid value being set"));
			return FALSE;
		}
		arg.d.char_data = value[0];
		break;
	case GTK_TYPE_UCHAR:
		if(!value[0]) {
			g_warning(_("Invalid value being set"));
			return FALSE;
		}
		arg.d.uchar_data = *((guchar *)value);
		break;
	case GTK_TYPE_BOOL:
		if(value[0] == 't' || value[0] == 'T' ||
		   value[0] == 'y' || value[0] == 'Y' ||
		   atoi(value) != 0)
			arg.d.bool_data = TRUE;
		else
			arg.d.bool_data = FALSE;
		break;
	case GTK_TYPE_INT:
		arg.d.int_data = atoi(value);
		break;
	case GTK_TYPE_UINT:
		arg.d.uint_data = strtoul(value, NULL, 10);
		break;
	case GTK_TYPE_LONG:
		arg.d.long_data = strtol(value, NULL, 10);
		break;
	case GTK_TYPE_ULONG:
		arg.d.ulong_data = strtoul(value, NULL, 10);
		break;
	case GTK_TYPE_FLOAT:
		arg.d.float_data = atof(value);
		break;
	case GTK_TYPE_DOUBLE:
		arg.d.double_data = atof(value);
		break;
	case GTK_TYPE_STRING:
		arg.d.string_data = (char *)value;
		break;
	case GTK_TYPE_ENUM:
		/* FIXME: this is wrong, we need to understand the basic
		 * gtk enums I guess, but even that is somewhat wrong */
		arg.d.int_data = atoi(value);
		break;
	
	default:
		g_warning(_("Unsupported argument type being set"));
		return FALSE;
	}

	gtk_object_arg_set(object, &arg, info);

	return TRUE;
}
