/* GnoP: Standard widget interface
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef GNOP_WIDGET_INTERFACE_H
#define GNOP_WIDGET_INTERFACE_H

#include <gtk/gtk.h>
#include <gconf/gconf-value.h>
#include "gnop-type.h" /* GnopType */

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Gnop Widgets should have this structure set as the data of name
 * "gnop_widget_interface"
 */

typedef struct _GnopWidgetInterface GnopWidgetInterface;
struct _GnopWidgetInterface {
	/* this is the name of the signal that should be bound to get changes
	 * on the widget, this signal should not have any extra function
	 * arguments (handler should look like
	 * "void foo(GtkWidget *widget, gpointer data)" ) */
	char *		changed_signal;

	gboolean	(*get_value)	(GtkWidget *w, GnopType type,
					 GConfValue **value);
	gboolean	(*set_value)	(GtkWidget *w, GConfValue *value);

	void		(*add_options)	(GtkWidget *w, GList *gnop_options);

	void		(*set_label)	(GtkWidget *w, const char *label);
};

/* This will create or get the current interface of a class, so derived
 * classes can easily override implementations of methods */
GnopWidgetInterface *
		gnop_widget_interface_add	(GtkObjectClass *klass);

/* check for the existance of the interface on a widget */
gboolean	gnop_widget_interface_exists	(GtkWidget *w);

const char *	gnop_widget_interface_changed_signal(GtkWidget *w);

gboolean	gnop_widget_interface_get_value	(GtkWidget *w,
						 GConfValueType type,
						 GConfValue **value);
gboolean	gnop_widget_interface_set_value	(GtkWidget *w,
						 GConfValue *value);

gboolean	gnop_widget_interface_implements_add_options(GtkWidget *w);
void		gnop_widget_interface_add_options(GtkWidget *w,
						  GList *gnop_options);

gboolean	gnop_widget_interface_implements_set_label(GtkWidget *w);
void		gnop_widget_interface_set_label	(GtkWidget *w,
						 const char *label);

#ifdef __cplusplus
}
#endif

#endif /* GNOP_WIDGET_INTERFACE_H */
