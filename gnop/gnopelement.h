/* GnoP: the elements interface
 * Author: George Lebl
 * (c) 1999 Free Software Foundation
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef GNOPELEMENT_H
#define GNOPELEMENT_H

#include <glib.h>
#include <gtk/gtk.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "gnop-type.h"

typedef struct _GnopElement GnopElement; 
typedef struct _GnopSensitivity GnopSensitivity; 

#ifdef __cplusplus
extern "C" {
#endif

/* Widge types */
typedef enum {
	/* the invalid widget */
	GNOP_INVALIDWIDGET,

	/* meta type for native widget,
	 * widgets using the gnop_widget_interface,
	 * used instead of specific types */
	GNOP_NATIVEWIDGET,

	/* number widgets */
	GNOP_GTKSPINBUTTON,
	GNOP_GNOMENUMBERENTRY,
	GNOP_GNOMECALCULATOR,

	/* string widgets */
	GNOP_GTKENTRY, /* can also be used for numbers */
	GNOP_GTKCOMBO,
	GNOP_GNOMEENTRY,
	GNOP_GNOMEFILEENTRY,
	GNOP_GNOMEPIXMAPENTRY,
	GNOP_GNOMEICONENTRY,

	/* boolean */
	GNOP_GTKTOGGLEBUTTON,
	GNOP_GTKCHECKBUTTON, /* the above is used for getting information */

	GNOP_LASTWIDGET
} GnopWidgetType;

/* type of comparison for sensitivity */
typedef enum {
	GNOP_COMP_INVALID = 0,
	GNOP_COMP_LT,
	GNOP_COMP_LE,
	GNOP_COMP_EQ,
	GNOP_COMP_NE,
	GNOP_COMP_GE,
	GNOP_COMP_GT,
	GNOP_COMP_LAST
} GnopComparison;
typedef enum {
	GNOP_CONNECTION_INVALID = 0,
	GNOP_CONNECTION_OR,
	GNOP_CONNECTION_AND,
	GNOP_CONNECTION_LAST
} GnopConnection;


struct _GnopElement {
	GnopType type;

	int refcount;

	/* configuration path to store setting at, this is also
	   used to index the gnop elements, and does not have to be
	   a complete path as the gnop-xml object can store the base
	   path */
	char *conf_path;

	/* user data */
	gpointer data;
	GDestroyNotify destroy_notify;

	/* the widget associated */
	char *widget;

	/* sensitivity elements */
	GSList *sensitivity;
};

/* for a single widget, when the value "value" is given the following
 * things should be set sensitive or insensitive */
struct _GnopSensitivity {
	GnopType type;
	GList *values;
	GnopConnection connection;
	GnopComparison comparison;
	GSList *sensitive;
	GSList *insensitive;
};


/* stores actual runtime information about a GnopElement */
typedef struct _GnopRuntimeElement GnopRuntimeElement;

typedef void (*GnopRuntimeUpdateFunc)(GnopRuntimeElement *, gpointer);

struct _GnopRuntimeElement {
	GnopElement *el;
	GtkWidget *widget;
	GnopWidgetType widget_type;

	guint gconf_notify;
	gboolean suspend_gconf_notify;

	/* if the widget/value was updated */
	gboolean updated;
	guint updated_idle;
	gboolean suspend_widget_update;

	GnopRuntimeUpdateFunc update_func;
	gpointer update_data;
};

/*
 * Element
 */
GnopElement *	gnop_element_new		(void);

void		gnop_element_set_type		(GnopElement *el,
						 GnopType type);
void		gnop_element_set_type_string	(GnopElement *el,
						 const char *type);
void		gnop_element_set_data		(GnopElement *el,
						 gpointer data,
						 GDestroyNotify destroy_notify);
void		gnop_element_set_widget		(GnopElement *el,
						 const char *widget);
void		gnop_element_set_conf_path	(GnopElement *el,
						 const char *conf_path);

void		gnop_element_ref		(GnopElement *ge);
void		gnop_element_unref		(GnopElement *ge);

/* Note: takes ownership of 'sens' */
void		gnop_element_add_sensitivity	(GnopElement *ge,
						 GnopSensitivity *sens);

/*
 * Sensitiviy
 */
GnopSensitivity	*gnop_sensitivity_new		(GnopElement *ge);
void		gnop_sensitivity_destroy	(GnopSensitivity *sens);

void		gnop_sensitivity_add_value	(GnopSensitivity *sens,
						 GConfValue *value);
void		gnop_sensitivity_add_value_from_string(GnopSensitivity *sens,
						       const char *str);
void		gnop_sensitivity_remove_value	(GnopSensitivity *sens,
						 GConfValue *value);
void		gnop_sensitivity_set_connection	(GnopSensitivity *sens,
						 GnopConnection connection);
void		gnop_sensitivity_set_comparison	(GnopSensitivity *sens,
						 GnopComparison comparison);
void		gnop_sensitivity_add_sensitive	(GnopSensitivity *sens,
						 const char *widget);
void		gnop_sensitivity_add_insensitive(GnopSensitivity *sens,
						 const char *widget);

/*
 * Runtime Element
 */
GnopRuntimeElement *
		gnop_runtime_element_new	(GnopElement *el,
						 GnopRuntimeUpdateFunc func,
						 gpointer data);
void		gnop_runtime_element_destroy	(GnopRuntimeElement *gre,
						 GConfClient *client);

void		gnop_runtime_element_add_notify	(GnopRuntimeElement *gre,
						 GConfClient *client,
						 const char *prefix,
						 GConfClientNotifyFunc func,
						 gpointer user_data,
						 GFreeFunc destroy_notify,
						 GConfError **err);
void		gnop_runtime_element_remove_notify(GnopRuntimeElement *gre,
						   GConfClient *client);

/* set widget and autodetect its type, returns FALSE if the
 * widget is not a valid widget */
gboolean	gnop_runtime_element_set_widget	(GnopRuntimeElement *el,
						 GtkWidget *widget);
/* setup the change handlers for the widget, not done in _set_widget since
 * we want to do _key_to_widget before setting the handlers */
void		gnop_runtime_element_setup_handlers(GnopRuntimeElement *gre);

/* Gets the value in the correct type according to the type of the GnopElement */
GConfValue *	gnop_runtime_element_get_value	(GnopRuntimeElement *gre);
void		gnop_runtime_element_set_value	(GnopRuntimeElement *gre, GConfValue *value);

/* sync widget to the key or the key to the widget */
void		gnop_runtime_element_widget_to_key(GnopRuntimeElement *gre,
						   GConfClient *client,
						   const char *prefix);
void		gnop_runtime_element_key_to_widget(GnopRuntimeElement *gre,
						   GConfClient *client,
						   const char *prefix);

/*
 * General functions
 */


/* remove any data that is not really needed to keep */
void		gnop_element_free_unused_data	(void);

#ifdef __cplusplus
}
#endif

#endif /* GNOPELEMENT_H */
