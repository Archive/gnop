/* GnoP: the pane interface
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef GNOPPANE_H
#define GNOPPANE_H

#include <glib.h>
#include <gtk/gtk.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "gnopparser.h"

#ifdef __cplusplus
extern "C" {
#endif

/* ui internal type */
#ifndef __TYPEDEF_GNOP_UI_INTERNAL__
#define __TYPEDEF_GNOP_UI_INTERNAL__
typedef struct _GnopUIInternal GnopUIInternal;
#endif

/* config stuff */
typedef struct _GnopPaneBase GnopPaneBase; 
typedef struct _GnopPane GnopPane; 
typedef struct _GnopGroup GnopGroup; 
typedef struct _GnopWidget GnopWidget; 
typedef struct _GnopArgument GnopArgument; 
typedef struct _GnopOption GnopOption; 

/* runtime stuff */
typedef struct _GnopDialog GnopDialog; 
typedef struct _GnopWidgetRuntime GnopWidgetRuntime;

struct _GnopPaneBase {
	int refcount;

	char *name;
	char *label;
	char **levels;

	GDestroyNotify destructor;
};

struct _GnopPane {
	GnopPaneBase base;

	GList *groups;
};

struct _GnopGroup {
	GnopPaneBase base;

	gboolean expand;

	/* FIXME: instead of set of GnopWidgets, this could be a glade
	 * file, so add something like that here, Also it should be
	 * possible to create one's own widgets */

	GList *widgets;
};

struct _GnopArgument {
	char *name;
	char *value;
};
struct _GnopOption {
	char *label;
	char *value;
};

struct _GnopWidget {
	GnopPaneBase base;

	char *tooltip;

	gboolean expand;

	int widget_type;
	GtkType widget_gtktype;

	GList *arguments;
	GList *options;
};

struct _GnopWidgetRuntime {
	GnopWidget *gnopwid;
	GtkWidget *wid;
};

struct _GnopDialog {
	int refcount;
	GtkWidget *wid;
	GtkWidget *pane_wid;
	GtkTooltips *tooltips;
	GList *panes;
	GHashTable *widgets;
	GHashTable *gtk_widgets;
};

/*
 * PaneBase
 */
void		gnop_pane_base_init		(GnopPaneBase *base,
						 GDestroyNotify destructor);

void		gnop_pane_base_ref		(GnopPaneBase *base);
void		gnop_pane_base_unref		(GnopPaneBase *base);

void		gnop_pane_base_set_name		(GnopPaneBase *base,
						 const char *name);
void		gnop_pane_base_set_label	(GnopPaneBase *base,
						 const char *label);
void		gnop_pane_base_set_levels	(GnopPaneBase *base,
						 const char **levels);
void		gnop_pane_base_set_levels_string(GnopPaneBase *base,
						 const char *levels);

/*
 * Pane
 */
GnopPane *	gnop_pane_new			(void);

void		gnop_pane_append_group		(GnopPane *pane,
						 GnopGroup *group);

/*
 * Group
 */
GnopGroup *	gnop_group_new			(void);

void		gnop_group_set_expand		(GnopGroup *group,
						 gboolean expand);

void		gnop_group_append_widget	(GnopGroup *group,
						 GnopWidget *widget);

/*
 * Widget
 */
GnopWidget *	gnop_widget_new			(void);

void		gnop_widget_set_expand		(GnopWidget *widget,
						 gboolean expand);
void		gnop_widget_set_widget_type	(GnopWidget *widget,
						 int widget_type);
void		gnop_widget_set_widget_gtktype	(GnopWidget *widget,
						 GtkType widget_gtktype);

void		gnop_widget_set_widget_type_string(GnopWidget *widget,
						   const char *widget_type);

void		gnop_widget_add_argument	(GnopWidget *widget,
						 const char *arg_name,
						 const char *arg_value);

void		gnop_widget_add_option		(GnopWidget *widget,
						 const char *opt_label,
						 const char *opt_value);

void		gnop_widget_set_tooltip         (GnopWidget *widget,
						 const char *tooltip);

/*
 * Dialog
 */
GnopDialog *	gnop_dialog_new			(GnopUIInternal *ui,
						 GnopFile *file,
						 const char *level);
void		gnop_dialog_ref			(GnopDialog *dialog);
void		gnop_dialog_unref		(GnopDialog *dialog);
GnopWidget *	gnop_dialog_lookup_widget	(GnopDialog *dialog,
						 const char *name);
GtkWidget *	gnop_dialog_lookup_gtk_widget	(GnopDialog *dialog,
						 const char *name);

/* Free all absolutely non essential stuff */
void		gnop_pane_free_unused_data	(void);

#ifdef __cplusplus
}
#endif

#endif /* GNOPPANE_H */
