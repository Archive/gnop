%at{
/* GnoP: the Internal GOB UI object
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
%}
requires 1.0.0

%{
#include "config.h"
#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "gnopparser.h"
#include "gnoppane.h"

#include "gnop-ui.h"

#include "gnop-ui-internal.h"
#include "gnop-ui-internal-private.h"

static void dialog_destroyed(GtkWidget *dialog, GnopUIInternal *self);
static gboolean dlg_keypress(GtkWidget *widget, GdkEventKey *event);

%}

class Gnop:UI:Internal from Gnop:UI {
	private GnopDialog *dialog = NULL
		destroywith gnop_dialog_unref;

	override (Gnop:UI)
	GtkWidget *
	get_widget(Gnop:UI *gi (check null type),
		   const char *name (check null))
	{
		Self *self = SELF(gi);

		if(self->_priv->dialog)
			return gnop_dialog_lookup_gtk_widget(self->_priv->dialog, name);

		return NULL;
	}

	override (Gnop:UI)
	void
	load_dialog(Gnop:UI *gi (check null type), GnopFile *gf (check null),
		    const char *level)
	{
		Self *self = SELF(gi);

		/* This should not really ever happen, but we're paranoid */
		if(self->_priv->dialog) {
			gtk_widget_destroy(self->_priv->dialog->wid);
			gnop_dialog_unref(self->_priv->dialog);
			self->_priv->dialog = NULL;
		}

		/* FIXME: if not auto_apply then we need to do the
		 * apply/ok/close dance, or maybe we should just not
		 * support non-auto_apply stuff */
		self->_priv->dialog = gnop_dialog_new(self, gf, level);

		if( ! self->_priv->dialog) {
			g_warning(_("Cannot make dialog"));
			return;
		}

		gtk_signal_connect(GTK_OBJECT(self->_priv->dialog->wid),
				   "key_press_event",
				   GTK_SIGNAL_FUNC(dlg_keypress), NULL);

		gtk_signal_connect_while_alive
			(GTK_OBJECT(self->_priv->dialog->wid), "destroy",
			 GTK_SIGNAL_FUNC(dialog_destroyed), self, 
			 GTK_OBJECT(self));

		gnop_ui_set_dialog_widget(gi, self->_priv->dialog->wid);
	}

	public
	GnopUI *
	new(void)
	{
		return GNOP_UI(GET_NEW);
	}

	public
	GtkWidget *
	get_pane_widget(self)
	{
		if(self->_priv->dialog)
			return self->_priv->dialog->pane_wid;
		else
			return NULL;
	}
}

%{
static void
dialog_destroyed(GtkWidget *dialog, GnopUIInternal *self)
{
	gnop_dialog_unref(self->_priv->dialog);
	self->_priv->dialog = NULL;
}

/* some evilness follows */
typedef struct _Fish Fish;
struct _Fish {
	int state;
	int x, y, xs, ys;
	GdkPixmap *fish[4];
	GdkBitmap *fish_mask[4];
	int handler;
	GdkWindow *win;
	GtkWidget *dialog;
};

static void
dlg_destroyed(GtkWidget *dialog, gpointer data)
{
	Fish *fish = data;
	int i;
	for(i = 0; i < 4; i++) {
		gdk_pixmap_unref(fish->fish[i]);
		gdk_bitmap_unref(fish->fish_mask[i]);
	}
	gtk_timeout_remove(fish->handler);
	memset(fish, 0, sizeof(Fish));
	g_free(fish);
}

/* I AAAAAM YOUUUUUUR FAAAAAAATTTTTHHHHHHHEEEEERRRRRR */
static gboolean
fish_move(gpointer data)
{
	Fish *fish = data;
	int orient, state;
	gboolean change = TRUE;

	fish->x += fish->xs;
	fish->y += fish->ys;
	if(fish->x <= 0 ||
	   fish->x >= fish->dialog->allocation.width - 60 ||
	   rand() % 50 == 0) {
		fish->xs = -fish->xs;
		change = TRUE;
	}
	if(fish->y <= 0 ||
	   fish->y >= fish->dialog->allocation.height - 40 ||
	   rand() % 50 == 0)
		fish->ys = -fish->ys;

	fish->state ++;
	if(fish->state % 4 == 0)
		change = TRUE;
	if(fish->state >= 8)
		fish->state = 0;

	state = fish->state >= 4 ? 1 : 0;
	orient = fish->xs >= 0 ? 0 : 2;

	if(change) {
		gdk_window_set_back_pixmap(fish->win, fish->fish[orient + state], FALSE);
		gdk_window_shape_combine_mask(fish->win, fish->fish_mask[orient + state], 0, 0);
		gdk_window_clear(fish->win);
	}

	gdk_window_move(fish->win, fish->x, fish->y);

	return TRUE;
}

static void
fish_reverse(GdkPixbuf *gp)
{
	guchar *pixels = gdk_pixbuf_get_pixels(gp);
	int x, y;
	int rs = gdk_pixbuf_get_rowstride(gp);
#define DOSWAP(x,y) tmp = x; x = y; y = tmp;
	for(y = 0; y < 40; y++, pixels += rs) {
		guchar *p = pixels;
		guchar *p2 = pixels + 60*4 - 4;
		for(x = 0; x < 30; x++, p+=4, p2-=4) {
			guchar tmp;
			DOSWAP(p[0],p2[0]);
			DOSWAP(p[1],p2[1]);
			DOSWAP(p[2],p2[2]);
			DOSWAP(p[3],p2[3]);
		}
	}
#undef DOSWAP
}

static void
fish_unwater(GdkPixbuf *gp)
{
	guchar *pixels = gdk_pixbuf_get_pixels(gp);
	int x, y;
	int rs = gdk_pixbuf_get_rowstride(gp);
	for(y = 0; y < 40; y++, pixels += rs) {
		guchar *p = pixels;
		for(x = 0; x < 60; x++, p+=4) {
			if(p[0] < 55 && p[1] > 100)
			       p[3] = 0;	
		}
	}
}

/* the incredibly evil function */
static void
doblah(GtkWidget *dialog)
{
	GdkWindowAttr attributes;
	char *fish_file;
	GdkPixbuf *gp, *tmp;
	Fish *fish;

	if(gtk_object_get_data(GTK_OBJECT(dialog), "fish"))
		return;

	fish_file = gnome_pixmap_file("fish/fishanim.png");
	if( ! fish_file)
		return;

	tmp = gdk_pixbuf_new_from_file(fish_file);
	if( ! tmp)
		return;

	g_free(fish_file);

	if(gdk_pixbuf_get_width(tmp) != 180 ||
	   gdk_pixbuf_get_height(tmp) != 40) {
		gdk_pixbuf_unref(tmp);
		return;
	}

	fish = g_new0(Fish, 1);
	fish->state = 0;
	fish->dialog = dialog;

	gp = gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8, 60, 40);
	gdk_pixbuf_copy_area(tmp, 60, 0, 60, 40, gp, 0, 0);

	fish_unwater(gp);
	gdk_pixbuf_render_pixmap_and_mask (gp, &fish->fish[2], &fish->fish_mask[2], 128);
	fish_reverse(gp);
	gdk_pixbuf_render_pixmap_and_mask (gp, &fish->fish[0], &fish->fish_mask[0], 128);

	gdk_pixbuf_copy_area(tmp, 120, 0, 60, 40, gp, 0, 0);

	fish_unwater(gp);
	gdk_pixbuf_render_pixmap_and_mask (gp, &fish->fish[3], &fish->fish_mask[3], 128);
	fish_reverse(gp);
	gdk_pixbuf_render_pixmap_and_mask (gp, &fish->fish[1], &fish->fish_mask[1], 128);
	gdk_pixbuf_unref(gp);

	gdk_pixbuf_unref(tmp);
	
	fish->x = (rand() % (dialog->allocation.width - 60 - 2)) + 1;
	fish->y = (rand() % (dialog->allocation.height - 40 - 2)) + 1;
	fish->xs = (rand() % 4) + 4;
	fish->ys = (rand() % 2) + 1;

	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.x = fish->x;
	attributes.y = fish->y;
	attributes.width = 60;
	attributes.height = 40;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.visual = gdk_rgb_get_visual();
	attributes.colormap = gdk_rgb_get_cmap();
	attributes.event_mask = 0;

	fish->win = gdk_window_new(dialog->window, &attributes,
				   GDK_WA_X | GDK_WA_Y |
				   GDK_WA_VISUAL | GDK_WA_COLORMAP);
	gdk_window_set_back_pixmap(fish->win, fish->fish[0], FALSE);
	gdk_window_shape_combine_mask(fish->win, fish->fish_mask[0], 0, 0);

	gdk_window_show(fish->win);
	fish->handler = gtk_timeout_add(150, fish_move, fish);
	gtk_signal_connect(GTK_OBJECT(dialog), "destroy",
			   GTK_SIGNAL_FUNC(dlg_destroyed), fish);
	gtk_object_set_data(GTK_OBJECT(dialog), "fish", fish);
}

/* only slightly evil function */
static gboolean
dlg_keypress(GtkWidget *widget, GdkEventKey *event)
{
	static int foo = 0;

	if( ! (event->state & GDK_CONTROL_MASK) ||
	    foo >= 4)
		return FALSE;

	switch (event->keyval) {
	case GDK_h:
	case GDK_H:
		if(foo == 3) { doblah(widget); } foo = 0; break;
	case GDK_s:
	case GDK_S:
		if(foo == 2) { foo++; } else { foo = 0; } break;
	case GDK_i:
	case GDK_I:
		if(foo == 1) { foo++; } else { foo = 0; } break;
	case GDK_f:
	case GDK_F:
		if(foo == 0) { foo++; } else { foo = 0; } break;
	default:
		foo = 0;
	}

	return FALSE;
}
%}
