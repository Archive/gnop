/* GnoP: the library interface
 * Author: George Lebl
 * (c) 1999 Free Software Foundation
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef GNOP_H
#define GNOP_H

#include "gnop-xml.h"

/* The standard (non-glade) ui objects */
#include "gnop-ui.h"
#include "gnop-ui-manual.h"
#include "gnop-ui-internal.h"

#ifdef __cplusplus
extern "C" {
#endif

/* initialize */
gboolean	gnop_is_initialized		(void);
gboolean	gnop_init			(gint argc, gchar **argv);

/* To be called at any time */
void		gnop_lazy_init			(void);

/* free any non essential data */
void		gnop_free_unused_data		(void);

/* add a directory to the path in which to look for gnop files */
void		gnop_add_directory		(const char *directory);

/* Pure sugar functions, this way adding a config dialog is a 1-liner */
GtkWidget *	gnop_run_simple_dialog		(const char *file);
GtkWidget *	gnop_run_simple_dialog_from_memory(const char *buffer, int len);

#ifdef __cplusplus
}
#endif

#endif /* GNOP_H */
