/* testgnop: gnop tester program
 * Author: George Lebl
 * (c) 1999 the Free Software Foundation
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "config.h"
#include <gnome.h>
/*#include <glade/glade.h>*/
#include <gnop/gnop.h>
/*#include <gnop/gnop-glade.h>*/
/*#include <gconf/gconf.h>
#include <gconf/gconf-client.h>*/

/*static GnopUI *ui = NULL;*/
static GnopXML *config = NULL;
/*static GConfClient *client = NULL;*/

static void
show_props(void)
{
	gnop_xml_show_dialog(config);
}

int
main(int argc, char *argv[])
{
	GtkWidget *app;
	GtkWidget *box;
	GtkWidget *w;
	GConfError* error = NULL;

	gnome_init("testgnop", "1.0", argc, argv);

	if( ! gconf_init(argc, argv, &error)) {
		g_assert(error != NULL);
		g_warning("GConf init failed:\n  %s", error->str);
		gconf_error_destroy(error);
		error = NULL;
		return 1;
	}

	if( ! gnop_init(argc, argv)) {
		g_warning("GnoP init failed!");
		return 1;
	}
	/*glade_gnome_init();*/

	app = gnome_app_new("testgnop", "Test Gnop");
	gtk_signal_connect(GTK_OBJECT(app), "destroy",
			   GTK_SIGNAL_FUNC(gtk_main_quit), NULL);

	box = gtk_vbox_new(FALSE, 0);

	w = gtk_button_new_with_label("Run property box");
	gtk_signal_connect(GTK_OBJECT(w), "clicked",
			   GTK_SIGNAL_FUNC(show_props), NULL);
	gtk_box_pack_start(GTK_BOX(box), w, FALSE, FALSE, 0);

	w = gtk_button_new_with_label("Quit");
	gtk_signal_connect(GTK_OBJECT(w), "clicked",
			   GTK_SIGNAL_FUNC(gtk_main_quit), NULL);
	gtk_box_pack_start(GTK_BOX(box), w, FALSE, FALSE, 0);

	gnome_app_set_contents(GNOME_APP(app), box);

	gtk_widget_show_all(app);

	/*client = gconf_client_get_default();*/

	/*
	ui = gnop_ui_glade_new_from_file("testgnop.glade", "testgnopbox");
	*/

	config = gnop_xml_new("testgnop.gnop");
	if( ! config)
		g_error("Can't load gnop file");

	gtk_main();

	gtk_object_unref(GTK_OBJECT(config));
	/*gtk_object_unref(GTK_OBJECT(client));*/
	
	return 0;
}
