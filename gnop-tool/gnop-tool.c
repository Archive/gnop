/* gnop-tool: gnop tool
 * Author: George Lebl
 * (c) 1999 the Free Software Foundation
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "config.h"
#include <string.h>
#include <gnome.h>
#include <glade/glade.h>
#include <gnop/gnop.h>
#include <gnop/gnop-glade.h>

static GnopUI *ui = NULL;
static GnopXML *config = NULL;

static gboolean use_glade = FALSE;
static char *glade_file = NULL;
static char *glade_dialog = NULL;
static char *prefix = NULL;
static char *level = NULL;

static const struct poptOption options[] = {
	{ "use-glade", 0, POPT_ARG_NONE, &use_glade, 0, N_("Use glade instead of internal dialog"), N_("USEGLADE") },
	{ "glade-file", 0, POPT_ARG_STRING, &glade_file, 0, N_("Glade file to use"), N_("GLADEFILE") },
	{ "glade-dialog", 0, POPT_ARG_STRING, &glade_dialog, 0, N_("Glade dialog to use"), N_("GLADEDIALOG") },
	{ "level", 0, POPT_ARG_STRING, &level, 0, N_("Gnop user level to use"), N_("LEVEL") },
	{ "prefix", 0, POPT_ARG_STRING, &prefix, 0, N_("Gnop prefix to use"), N_("GNOPPREFIX") },
	{ NULL } 
};

int
main(int argc, char *argv[])
{
	poptContext ctx;
	GtkWidget *dialog;
	GConfError* error = NULL;
	const char **args;
	const char *file;

	gnome_init_with_popt_table("gnop-tool", VERSION, argc, argv, options, 0, &ctx);

	if( ! gconf_init(argc, argv, &error)) {
		g_assert(error != NULL);
		g_warning(_("GConf init failed:\n  %s"), error->str);
		gconf_error_destroy(error);
		error = NULL;
		return 1;
	}

	if( ! gnop_init(argc, argv))
		g_error(_("GnoP init failed!"));

	glade_gnome_init();
	
	args = poptGetArgs (ctx);

	if( ! args ||
	    args[0] == NULL ||
	    args[1] != NULL)
		g_error(_("You must supply a single argument with the .gnop file to use"));

	file = args[0];

	if( ! g_file_exists(file))
		g_error(_("'%s' does not exist!"), file);

	if(use_glade) {
		if( ! glade_dialog)
			glade_dialog = "dialog";
		if( ! glade_file) {
			int len = strlen(file);
			if(strcmp(&file[len - strlen(".gnop")], ".gnop") == 0) {
				glade_file = g_new(char,
						   len + 1 +
						   (strlen(".glade") -
						    strlen(".gnop")));
				strcpy(glade_file, file);
				strcpy(&glade_file[len - strlen(".gnop")], ".glade");
			} else {
				glade_file = g_strconcat(file, ".glade", NULL);
			}
		}

		ui = gnop_ui_glade_new_from_file(glade_file, glade_dialog);

		if( ! ui)
			g_error(_("Error in loading glade file"));
	}

	if(ui)
		config = gnop_xml_new_with_ui(file, ui);
	else
		config = gnop_xml_new(file);
	if( ! config)
		g_error(_("Can't load gnop file: %s"), file);

	if(level)
		gnop_xml_set_level(config, level);

	if(prefix)
		gnop_xml_set_prefix(config, prefix);

	gnop_xml_show_dialog(config);

	dialog = gnop_xml_get_dialog_widget(config);
	if( ! dialog) {
		gtk_object_unref(GTK_OBJECT(config));
		g_error(_("Can't run dialog"));
	}

	gtk_signal_connect(GTK_OBJECT(dialog), "destroy",
			   GTK_SIGNAL_FUNC(gtk_main_quit),
			   NULL);
	gtk_signal_connect(GTK_OBJECT(dialog), "delete_event",
			   GTK_SIGNAL_FUNC(gtk_false),
			   NULL);

	gtk_main();

	gtk_object_unref(GTK_OBJECT(config));
	
	return 0;
}
