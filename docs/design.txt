GnoP = Gnome Properties

GnoP is a library that eases writing configuration dialogs.  It uses gconf as
the backend and works in a mode where changes are done immediately.

The GnoP file is an xml file which describes the different options.  The user
interface is done in several modes

  1) Internal, this is the Pane mode.  This is the prefered mode of operation
  2) Manual, you manually create the entire dialog and just let GnoP connect
     the widgets to gconf
  3) Glade, the dialog is described in a glade file.

Connecting to Widgets
---------------------

GnoP connects a single gconf entry to a single widget.  If anything custom is
needed, then either you have to write your own widget conforming to the gnop
widget interface, or you have to code that part of the dialog and connect it
manually.

The Pane Mode
-------------

In this mode the interface is described in the gnop xml file itself.  The
interface works as follows:  The dialog contains a list of panes, (these are
identical to notebook tabs).  Each pane has a list of groups, which are
represented as frames.  Each frame has a list of widgets.

You can bind into the dialog creation and add your own interface and custom
connection.

Look at gnop/testgnop.gnop for an example file and gnop/testgnop.c for an
example of the use.
