#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="(GNOP) GNOME Properties"

(test -f $srcdir/configure.in \
  && test -d $srcdir/gnop \
  && test -d $srcdir/gnop-edit) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gnome properties directory"
    exit 1
}

. $srcdir/macros/autogen.sh
